package cdac.nvli.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SearchResult implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String recordIdentifier;
    private List<String> identifier;
    private List<String> title;
    private List<String> description;
    private List<String> data;
    private List<String> creator;
    private List<String> publisher;
    private List<String> language;
    private List<String> subject;
    private List<String> source;
    private String metaType;
    private List<String> contributor;
    private List<String> udcNotation;
    private List<String> type;
    private long recordLikeCount;
    private long recordViewCount;
    private long recordNoOfDownloads;
    private long ratingCount;
    private long recordNoOfShares;
    private long recordNoOfReview;
    private boolean crowdSourceFlag;
    private boolean crowdSourceStageFlag;
    private Date updatedOn;
    private List<String> customNotation;
    private double ratioOfLikeView;
    private String subResourceIdentifier;
    private String resourceType;
    private String imagePath;
    private String nameToView;
    private float defaultScore;
    private float nvliScore = 0;
    private float ratingScore;

    public float getRatingScore() {
        return ratingScore;
    }

    public void setRatingScore(float ratingScore) {
        this.ratingScore = ratingScore;
    }

    public float getNvliScore() {
        return nvliScore;
    }

    public void setNvliScore(float nvliScore) {
        this.nvliScore = nvliScore;
    }

    public float getDefaultScore() {
        return defaultScore;
    }

    public void setDefaultScore(float defaultScore) {
        this.defaultScore = defaultScore;
    }

    public String getNameToView() {
        return nameToView;
    }

    public void setNameToView(String nameToView) {
        this.nameToView = nameToView;
    }

    public long getRecordNoOfReview() {
        return recordNoOfReview;
    }

    public void setRecordNoOfReview(long recordNoOfReview) {
        this.recordNoOfReview = recordNoOfReview;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public long getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(long ratingCount) {
        this.ratingCount = ratingCount;
    }

    public long getRecordNoOfShares() {
        return recordNoOfShares;
    }

    public void setRecordNoOfShares(long recordNoOfShares) {
        this.recordNoOfShares = recordNoOfShares;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getSubResourceIdentifier() {
        return subResourceIdentifier;
    }

    public void setSubResourceIdentifier(String subResourceIdentifier) {
        this.subResourceIdentifier = subResourceIdentifier;
    }

    public double getRatioOfLikeView() {
        return ratioOfLikeView;
    }

    public void setRatioOfLikeView(double ratioOfLikeView) {
        this.ratioOfLikeView = ratioOfLikeView;
    }

    public List<String> getCustomNotation() {
        return customNotation;
    }

    public void setCustomNotation(List<String> customNotation) {
        this.customNotation = customNotation;
    }

    public boolean isCrowdSourceFlag() {
        return crowdSourceFlag;
    }

    public void setCrowdSourceFlag(boolean crowdSourceFlag) {
        this.crowdSourceFlag = crowdSourceFlag;
    }

    public boolean isCrowdSourceStageFlag() {
        return crowdSourceStageFlag;
    }

    public void setCrowdSourceStageFlag(boolean crowdSourceStageFlag) {
        this.crowdSourceStageFlag = crowdSourceStageFlag;
    }

    public long getRecordLikeCount() {
        return recordLikeCount;
    }

    public void setRecordLikeCount(long recordLikeCount) {
        this.recordLikeCount = recordLikeCount;
    }

    public long getRecordNoOfDownloads() {
        return recordNoOfDownloads;
    }

    public void setRecordNoOfDownloads(long recordNoOfDownloads) {
        this.recordNoOfDownloads = recordNoOfDownloads;
    }

    public long getRecordViewCount() {
        return recordViewCount;
    }

    public void setRecordViewCount(long recordViewCount) {
        this.recordViewCount = recordViewCount;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public List<String> getContributor() {
        return contributor;
    }

    public void setContributor(List<String> contributor) {
        this.contributor = contributor;
    }

    public List<String> getCreator() {
        return creator;
    }

    public void setCreator(List<String> creator) {
        this.creator = creator;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> date) {
        this.data = date;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public List<String> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(List<String> identifier) {
        this.identifier = identifier;
    }

    public List<String> getLanguage() {
        return language;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }

    public String getMetaType() {
        return metaType;
    }

    public void setMetaType(String metaType) {
        this.metaType = metaType;
    }

    public List<String> getPublisher() {
        return publisher;
    }

    public void setPublisher(List<String> publisher) {
        this.publisher = publisher;
    }

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public List<String> getSource() {
        return source;
    }

    public void setSource(List<String> source) {
        this.source = source;
    }

    public List<String> getSubject() {
        return subject;
    }

    public void setSubject(List<String> subject) {
        this.subject = subject;
    }

    public List<String> getTitle() {
        return title;
    }

    public void setTitle(List<String> title) {
        this.title = title;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public List<String> getUdcNotation() {
        return udcNotation;
    }

    public void setUdcNotation(List<String> udcNotation) {
        this.udcNotation = udcNotation;
    }
}
