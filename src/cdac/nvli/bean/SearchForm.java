package cdac.nvli.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


public class SearchForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * search element
     */
    private String searchElement;
    /**
     * list of SearchResult
     */
    private List<SearchResult> listOfResult;
    /**
     * list of subResourceIdentifiers
     */
    private List<String> subResourceIdentifiers;
    /**
     * size of result
     */
    private Long resultSize;
    /**
     * result found or not
     */
    private boolean resultFound;
    /**
     * for holding faceting result
     */
    private Map<String, List<FacetNameCountHolder>> facetMap;
    /**
     * for holding faceting result
     */
    private Map<String, List<String>> filters;
    /**
     * all resources faceting
     */
    private Map<String, Long> allResources;
    /**
     * resource Code
     */
    private String resourceCode;

    /**
     * for holding specific faceting result
     */
    private Map<String, List<FacetNameCountHolder>> specificFacetMap;
    /**
     * for holding specific faceting filter result
     */
    private Map<String, List<String>> specificFilters;
    /**
     * for which ranking rule to apply
     */
    private String rankingRule;

    /**
     * To hold search is basic or advance
     */
    private SearchComplexityEnum searchComplexity;
    /**
     * advance search string
     */
    private String advanceSearchQueryString;

    /**
     * From date in string format
     */
    private String fromDate;
    /**
     * To date in string format
     */
    private String toDate;

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public SearchComplexityEnum getSearchComplexity() {
        return searchComplexity;
    }

    public void setSearchComplexity(SearchComplexityEnum searchComplexity) {
        this.searchComplexity = searchComplexity;
    }

    public String getAdvanceSearchQueryString() {
        return advanceSearchQueryString;
    }

    public void setAdvanceSearchQueryString(String advanceSearchQueryString) {
        this.advanceSearchQueryString = advanceSearchQueryString;
    }

    public String getRankingRule() {
        return rankingRule;
    }

    public void setRankingRule(String rankingRule) {
        this.rankingRule = rankingRule;
    }

    public Map<String, List<FacetNameCountHolder>> getSpecificFacetMap() {
        return specificFacetMap;
    }

    public void setSpecificFacetMap(Map<String, List<FacetNameCountHolder>> specificFacetMap) {
        this.specificFacetMap = specificFacetMap;
    }

    public Map<String, List<String>> getSpecificFilters() {
        return specificFilters;
    }

    public void setSpecificFilters(Map<String, List<String>> specificFilters) {
        this.specificFilters = specificFilters;
    }

    public String getSearchElement() {
        return searchElement;
    }

    public void setSearchElement(String searchElement) {
        this.searchElement = searchElement;
    }

    public List<SearchResult> getListOfResult() {
        return listOfResult;
    }

    public void setListOfResult(List<SearchResult> listOfResult) {
        this.listOfResult = listOfResult;
    }

    public List<String> getSubResourceIdentifiers() {
        return subResourceIdentifiers;
    }

    public void setSubResourceIdentifiers(List<String> subResourceIdentifiers) {
        this.subResourceIdentifiers = subResourceIdentifiers;
    }

    public Long getResultSize() {
        return resultSize;
    }

    public void setResultSize(Long resultSize) {
        this.resultSize = resultSize;
    }

    public boolean isResultFound() {
        return resultFound;
    }

    public void setResultFound(boolean resultFound) {
        this.resultFound = resultFound;
    }

    public Map<String, List<FacetNameCountHolder>> getFacetMap() {
        return facetMap;
    }

    public void setFacetMap(Map<String, List<FacetNameCountHolder>> facetMap) {
        this.facetMap = facetMap;
    }

    public Map<String, List<String>> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, List<String>> filters) {
        this.filters = filters;
    }

    public Map<String, Long> getAllResources() {
        return allResources;
    }

    public void setAllResources(Map<String, Long> allResources) {
        this.allResources = allResources;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

}
