package cdac.nvli.bean;

public class FacetNameCountHolder {
    /**
     * Name Value of Facet
     */
    private String name;
    /*
     * Count of the records   
     */
    private int count;
    
    /**
     * Getter of count
     * @return 
     */
    public int getCount() {
        return count;
    }
    /**
     * setter of count
     * @param count 
     */
    public void setCount(int count) {
        this.count = count;
    }
    /**
     * Getter of name
     * @return 
     */
    public String getName() {
        return name;
    }
    /**
     * Setter of name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }
}
