package cdac.nvli.client;

import cdac.nvli.bean.SearchComplexityEnum;
import cdac.nvli.bean.SearchForm;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author Loveyj@aai_cdacp
 */
public class ClientTester {

	public static void main(String[] args) throws URISyntaxException,
			ClientProtocolException, IOException {
		String plainCreds = "MetadataIntegratorUser:mit_user@123#";
		
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		HttpClient httpClient = new DefaultHttpClient();

/* for date conversion checking
 * 			String date="03 November 1947";
			SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			dateParser.setTimeZone(TimeZone.getTimeZone("Z"));
			SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMMM yyyy");
            try {
				System.out.println("olddateparser"+oldDateParser.parse(date));
				System.out.println("newDateparser"+dateParser.format(oldDateParser.parse(date)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		
		/*String url = "http://clia3.pune.cdac.in:6019/NVLIRestWebService/rest/search/param";*/
		// String url = "http://clia3.pune.cdac.in:8668/NVLIRestWebService/webservice/basicSearch/1/20";
			
		 
		 /*	//String url = "http://clia3.pune.cdac.in:8668/NVLIRestWebService/webservice/DIDYOUMEAN";
		 * 
		 * */
		//String url = "http://clia3:8668/NVLIRestWebService/webservice/vasSearch/1/20";
		//String url = "http://clia3.pune.cdac.in:8668/NVLIRestWebService/webservice/basicSearch/1/20";
		//String url = "http://clia3:6100/NVLIRestWebService/webservice/basicSearch/1/50?language=hi";
		//String url="http://clia3.pune.cdac.in:8668/snacktoryWebService/webservice/GETDATA/en.wikipedia.org@wiki@Begum_Akhtar_Sulaiman";
		/*String url = "http://clia16.pune.cdac.in:8989/NVLIRestWebService/webservice/crosslingual/pa/ਭਾਰਤੀ";*/
		//String url = "http://clia16:8989/NVLIRestWebService/webservice/vasSearch/1/20";
		
	//	String url = "http://clia16:8989/NVLIRestWebService/webservice/vasSearch/1/20";
		String url = "http://10.80.4.91:8989/NVLIRestWebService/webservice/basicSearch/1/20";
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("Authorization", base64Creds);
		httpPost.addHeader("accept", "application/json");
		SearchForm s = new SearchForm();
		s.setSearchElement("Modi");
		s.setResourceCode("WIKI");
		s.setSearchComplexity(SearchComplexityEnum.BASIC);
		
		List<String> ls=new ArrayList<>();
		/*ls.add("VA_AWD");
		ls.add("VA_CON");
	*/	ls.add("WIKIEN");
		
		Map<String, List<String>> hm=new HashMap<>();
		//hm.put("hostname", ls);
		hm.put("wikicode", ls);
		List<String> ls2=new ArrayList<>();
		ls2.add("en");
 		//hm.put("languages", ls2); 
		List<String> ls3=new ArrayList<>();
		ls3.add("Seminars");
		//hm.put("vascode", ls3);
		//s.setSubResourceIdentifiers(ls);
		//s.setFilters(hm);
		//s.setSpecificFilters(hm);
		StringEntity entity = new StringEntity(new GsonBuilder().create()
				.toJson(s, SearchForm.class));
		entity.setContentType("application/json");
		//StringEntity entity = new StringEntity("Switrerland");
		entity.setContentType("application/json");
		httpPost.setEntity(entity);

		HttpResponse response = httpClient.execute(httpPost);
		System.out.println("Response: "+response);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));
		String line = "";
		String builder = new String();
		while ((line = rd.readLine()) != null) {
			builder+=line;
			builder+="\n";
		}

		Gson g = new GsonBuilder().generateNonExecutableJson().setPrettyPrinting().create();

		System.out.println("---" + g.toJson(builder));

		
		/*System.out.println("Hulalalal: ");
		String s2="PUNJAB_PUNJABKESARI_IN_PATIALA_201723010404";
		String s3="PUNJABKESARI";
		if(s2.contains(s3))
			System.out.println("true"+s3);
		else
			System.out.println("false"+s3);
		
		for(String s1:plainCreds.split("|"))
		{
			System.out.println("S:"+s1);
		}*/
		// String url =
		// "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/get/tags/CustomTagging/NVLI_MUOFIN_alh_ald-AM-MIN-1582-3516_1463204613709";
		// HttpEntity requestpost = new HttpEntity(headers);
		// ResponseEntity<TagDetails> b1 =restTemplate.exchange(url,
		// org.springframework.http.HttpMethod.GET,requestpost,TagDetails.class);
		// TagDetails tagDetails=b1.getBody();
		// System.out.println("---"+tagDetails.getTagValue());
		//
		// tagDetails.setTagValue("saasasassas");
		//
		//
		//
		// url =
		// "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/add/tags";
		//
		// requestpost = new HttpEntity(tagDetails, headers);
		// ResponseEntity<Boolean> b = restTemplate.postForEntity(new
		// URI(url),requestpost,Boolean.class);
		// System.out.println("=========="+b.getBody());

		// String url =
		// "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/crowdsource/NVLI_MANUPT_ignca-sb8472-ms_1_1463209917029";
		// HttpEntity<String> requestpost = new HttpEntity(headers);
		// ResponseEntity<MARC21MetadataStandard> responseEntity =
		// restTemplate.exchange(url,
		// org.springframework.http.HttpMethod.GET,requestpost,MARC21MetadataStandard.class);
		// MARC21MetadataStandard d=responseEntity.getBody();
		// System.out.println(d.getRecordIdentifier());
		// System.out.println(d.getIsCloseForEdit());
		//
		// d.setIsCloseForEdit("no");
		//
		//
		// String url1 =
		// "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/crowdsource/update/marc21";
		// HttpEntity requestpost1 = new HttpEntity(d,headers);
		// ResponseEntity<Void> responseEntity1 =
		// restTemplate.postForEntity(url1, requestpost1,Void.class);

		// url =
		// "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/crowdsource/NVLI_DLIIND_book-1_1463206062991";
		// HttpEntity<String> requestpost = new HttpEntity(headers);
		// ResponseEntity<DCMetadataStandard> responseEntity =
		// restTemplate.exchange(url, org.springframework.http.HttpMethod.GET,
		// requestpost, DCMetadataStandard.class);
		// DCMetadataStandard dCMetadataStandard = responseEntity.getBody();
		// System.out.println("DCMETADATASTANDARD-->" +
		// dCMetadataStandard.getRecordIdentifier() + "---" +
		// dCMetadataStandard.getIsCloseForEdit());
		// //dCMetadataStandard.setIsCloseForEdit("no");
		// List<String> title=new ArrayList<>();
		// title.add("science");
		// dCMetadataStandard.getMetadata().setTitle(title);
		//
		// url =
		// "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/crowdsource/update/dc";
		// HttpEntity requestpost1 = new HttpEntity(dCMetadataStandard,
		// headers);
		// ResponseEntity<Boolean> responseEntity1 =
		// restTemplate.postForEntity(url, requestpost1, Boolean.class);
		// System.out.println("DCMETADA AFTEUPDATE-->" +
		// responseEntity1.getBody());

		// MARC21MetadataStandard d=responseEntity.getBody();

		// url =
		// "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/information/all/MOI_1462422684740_1462423050685";
		// ResponseEntity<SubResourceDetail> responseEntity =
		// restTemplate.exchange(url, HttpMethod.GET, request,
		// SubResourceDetail.class);
		// SubResourceDetail sd = responseEntity.getBody();
		// ResourceInformation rinfo =
		// responseEntity.getBody().getResourceInformation();
		// SubresourceInformation srinfo =
		// responseEntity.getBody().getSubResourceInformation();
		// rinfo.setResourceCode("JAI_MATA_DI");
		//
		// srinfo.setActivationFlag(true);
		// srinfo.setEmailId("kuch@kuch.kcu");
		//
		// headers.setContentType(MediaType.APPLICATION_JSON);
		//
		// HttpEntity requestpost = new HttpEntity(sd, headers);
		// url =
		// "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/save";
		// SubResourceDetail responseEntity1 = restTemplate.postForObject(url,
		// requestpost, SubResourceDetail.class);
		// System.out.println(responseEntity1);
		// url =
		// "http://localhost:8080/metadata-integrator-rest/webservice/popular/records/rarebooks_1462767030866";
		// ResponseEntity<List> responseEntity = restTemplate.exchange(url,
		// HttpMethod.GET, request, List.class);
		// Gson g = new Gson();
		// List<Object> list = responseEntity.getBody();
		// System.out.println("" + list.size());
		// System.out.println("" + list);
		// Iterator it = list.iterator();
		// int i = 0;
		// while (i < list.size()) {
		// LinkedHashMap hashMap = (LinkedHashMap) list.get(i);
		// System.out.println(hashMap.get("recordLikeCount") + "" +
		// hashMap.get("recordIdentifier"));
		// i++;
		// }

		// To check update record like or view or download count
		// NVLI_PRIYA5_koha_1_1463114731390
		// String r = "NVLI_PRIYA5_koha_1_1463114731390";
		// HttpHeaders headers1 = new HttpHeaders();
		// headers1.add("Authorization", "Basic " + base64Creds);
		// headers1.add("recordIdentifiers", r);
		// HttpEntity<String> request1 = new HttpEntity(headers1);
		// RestTemplate restTemplate1 = new RestTemplate();
		// String url1 =
		// "http://localhost:8080/metadata-integrator-rest/webservice/metadata/update/like";
		// ResponseEntity<Void> responseEntity1 = restTemplate.exchange(url1,
		// HttpMethod.GET, request1, Void.class);
	}
	// Gson gson = new Gson();
	// Use follwing way to access url of put method dont remove it
	// TagDetails details = new TagDetails();
	// details.setRecordIdentifier("NVLI_RAREBK_book-1_meta_1462954970716");
	// details.setTagType(Constants.UDCTAGGING);
	// details.setTagValue("599.62&|&336.2&|&71");
	// HttpEntity<String> request = new HttpEntity(details, headers);
	// url =
	// "http://localhost:8080/metadata-integrator-rest/webservice/add/tags";
	// restTemplate.put(url, request);
	// Use to call most popularrecord method
	// url =
	// "http://localhost:8080/metadata-integrator-rest/webservice/popular/records/rarebooks_1462767030866";
	// ResponseEntity<List> responseEntity = restTemplate.exchange(url,
	// HttpMethod.GET, request, List.class);
	// Gson g = new Gson();
	// List<Object> list = responseEntity.getBody();
	// System.out.println("" + list.size());
	// System.out.println("" + list);
	// Iterator it = list.iterator();
	// int i = 0;
	// while (i < list.size()) {
	// LinkedHashMap hashMap = (LinkedHashMap) list.get(i);
	// System.out.println(hashMap.get("recordLikeCount") + "" +
	// hashMap.get("recordIdentifier"));
	// i++;
	// }
	// url =
	// "http://localhost:8080/metadata-integrator-rest/webservice/crowdsource/get/metadata/NVLI_RAREBK_alh_ald-AM-MIN-603-1700_1463049798003";
	// ResponseEntity<String> responseEntity = restTemplate.exchange(url,
	// HttpMethod.GET, request, String.class);
	// System.out.println(responseEntity.getBody());
}