package cdac.nvli.util.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.lucene.search.spell.PlainTextDictionary;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.FSDirectory;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;

import cdac.nvli.util.service.QueryTranslationService;
import org.gauner.jSpellCorrect.ToySpellingCorrector;

/**
 * @author Lovey@cdacpune
 *
 */

public class ProcessingDao {

	// varriables
	public static SpellChecker sp;
	public static ToySpellingCorrector sc = null;
	public SolrQuery solrquery;
	public QueryResponse queryResponse;
	public HttpSolrServer httpSolrServer;
	private Properties prop = new Properties();
	private InputStream input = null;
	public static Logger logger = Logger.getLogger("SearchResources");
	private static String NE_DICTIONARY_LOCATION = "";
	private static String TRANSLATION_DICTIONARY_LOCATION = "";
	private static String TRANSLITERATION_DICTIONARY_LOCATION = "";

	private static List<String> language_supported;

	private static Map<String, String> NE_DICTIONARY_MAP_EN = new HashMap<>();
	private static Map<String, String> TRANSLATION_DICTIONARY_MAP_EN = new HashMap<>();
	private static Map<String, String> TRANSLITERATION_DICTIONARY_MAP_EN = new HashMap<>();
	private static Map<String, String> NE_DICTIONARY_MAP_HI = new HashMap<>();
	private static Map<String, String> TRANSLATION_DICTIONARY_MAP_HI = new HashMap<>();
	private static Map<String, String> TRANSLITERATION_DICTIONARY_MAP_HI = new HashMap<>();
	private static QueryTranslationService queryTranslationService = null;

	public final static String HIGHLIGHTER_CSS_START_TAG = "<mark>";
	public final static String HIGHLIGHTER_CSS_END_TAG = "</mark>";

	/**
	 * ProcessingDao implementation for general data-access object utilizations
	 * 
	 * @return void
	 * @author Lovey Joshi
	 * 
	 */
	public ProcessingDao() {
		logger.log(Level.INFO, "ProcessingDao Initialized");
		input = ProcessingDao.class.getClassLoader().getResourceAsStream("serverconfig.properties");
		if (input != null) {
			try {
				prop.load(input);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				throw new FileNotFoundException("property file '" + input + "' not found in the classpath");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.createSpellCheckerDictionary(prop.get("BIG_FILE_FOR_SPELL_CHECK").toString());

		if (language_supported == null || language_supported.isEmpty()) {
			String[] spiltLanguages = prop.get("LANGUAGES_SUPPORTED").toString().split(",");
			language_supported = new ArrayList<>();
			language_supported = Arrays.asList(spiltLanguages);
		}
		NE_DICTIONARY_LOCATION = prop.getProperty("NE_DICTIONARY_LOCATION");
		TRANSLATION_DICTIONARY_LOCATION = prop.getProperty("TRANSLATION_DICTIONARY_LOCATION");
		TRANSLITERATION_DICTIONARY_LOCATION = prop.getProperty("TRANSLITERATION_DICTIONARY_LOCATION");

		if (NE_DICTIONARY_MAP_EN.isEmpty()) {
			for (String lang : language_supported) {
				try {
					BufferedReader bfr = new BufferedReader(new FileReader(
							new File(NE_DICTIONARY_LOCATION + "/" + lang.toLowerCase() + "ToenTranslation.txt")));

					String readline;
					while ((readline = bfr.readLine()) != null) {

						String[] readparts = readline.split("\t");
						// logger.log(Level.INFO, readparts[0]+" &
						// "+readparts[1]);
						try {
							NE_DICTIONARY_MAP_EN.put(readparts[0].trim(), readparts[1].trim());
						} catch (ArrayIndexOutOfBoundsException e) {
							// e.printStackTrace();
						}
					}

					bfr.close();
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}

			}
			logger.log(Level.INFO,
					"Total entries in NE Map for IL-English:" + String.valueOf(NE_DICTIONARY_MAP_EN.size()));
		}
		if (TRANSLATION_DICTIONARY_MAP_EN.isEmpty()) {
			for (String lang : language_supported) {
				try {
					BufferedReader bfr = new BufferedReader(new FileReader(new File(
							TRANSLATION_DICTIONARY_LOCATION + "/" + lang.toLowerCase() + "ToenTranslation.txt")));

					String readline;
					while ((readline = bfr.readLine()) != null) {

						String[] readparts = readline.split("\t");
						// logger.log(Level.INFO, readparts[0]+" &
						// "+readparts[1]);
						try {
							TRANSLATION_DICTIONARY_MAP_EN.put(readparts[0].trim(), readparts[1].trim());
						} catch (ArrayIndexOutOfBoundsException e) {
							// e.printStackTrace();
						}
					}

					bfr.close();
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}

			}
			logger.log(Level.INFO, "Total entries in Translation Map for IL-English:"
					+ String.valueOf(TRANSLATION_DICTIONARY_MAP_EN.size()));

		}
		if (TRANSLITERATION_DICTIONARY_MAP_EN.isEmpty()) {
			for (String lang : language_supported) {
				try {
					BufferedReader bfr = new BufferedReader(new FileReader(new File(TRANSLITERATION_DICTIONARY_LOCATION
							+ "/" + lang.toLowerCase() + "ToenTransliteration.txt")));

					String readline;
					while ((readline = bfr.readLine()) != null) {

						String[] readparts = readline.split("\t");
						// logger.log(Level.INFO, readparts[0]+" &
						// "+readparts[1]);
						try {
							TRANSLITERATION_DICTIONARY_MAP_EN.put(readparts[0].trim(), readparts[1].trim());
						} catch (ArrayIndexOutOfBoundsException e) {
							// e.printStackTrace();
						}
					}

					bfr.close();
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}

			}
			logger.log(Level.INFO, "Total entries in Transliteration Map for IL-English:"
					+ String.valueOf(TRANSLITERATION_DICTIONARY_MAP_EN.size()));

		}
		if (NE_DICTIONARY_MAP_HI.isEmpty()) {
			for (String lang : language_supported) {
				try {
					BufferedReader bfr = new BufferedReader(new FileReader(
							new File(NE_DICTIONARY_LOCATION + "/" + lang.toLowerCase() + "TohiTranslation.txt")));

					String readline;
					while ((readline = bfr.readLine()) != null) {

						String[] readparts = readline.split("\t");
						// logger.log(Level.INFO, readparts[0]+" &
						// "+readparts[1]);
						try {
							NE_DICTIONARY_MAP_HI.put(readparts[0].trim(), readparts[1].trim());
						} catch (ArrayIndexOutOfBoundsException e) {
							// e.printStackTrace();
						}
					}

					bfr.close();
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}

			}
			logger.log(Level.INFO,
					"Total entries in NE Map for IL-Hindi:" + String.valueOf(NE_DICTIONARY_MAP_HI.size()));

		}
		if (TRANSLATION_DICTIONARY_MAP_HI.isEmpty()) {
			for (String lang : language_supported) {
				try {
					BufferedReader bfr = new BufferedReader(new FileReader(new File(
							TRANSLATION_DICTIONARY_LOCATION + "/" + lang.toLowerCase() + "TohiTranslation.txt")));

					String readline;
					while ((readline = bfr.readLine()) != null) {

						String[] readparts = readline.split("\t");
						// logger.log(Level.INFO, readparts[0]+" &
						// "+readparts[1]);
						try {
							TRANSLATION_DICTIONARY_MAP_HI.put(readparts[0].trim(), readparts[1].trim());
						} catch (ArrayIndexOutOfBoundsException e) {
							// e.printStackTrace();
						}
					}

					bfr.close();
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}

			}
			logger.log(Level.INFO, "Total entries in Translation Map for IL-Hindi:"
					+ String.valueOf(TRANSLATION_DICTIONARY_MAP_HI.size()));

		}
		if (TRANSLITERATION_DICTIONARY_MAP_HI.isEmpty()) {
			for (String lang : language_supported) {
				try {
					BufferedReader bfr = new BufferedReader(new FileReader(new File(TRANSLITERATION_DICTIONARY_LOCATION
							+ "/" + lang.toLowerCase() + "TohiTransliteration.txt")));

					String readline;
					while ((readline = bfr.readLine()) != null) {

						String[] readparts = readline.split("\t");
						// logger.log(Level.INFO, readparts[0]+" &
						// "+readparts[1]);
						try {
							TRANSLITERATION_DICTIONARY_MAP_HI.put(readparts[0].trim(), readparts[1].trim());
						} catch (ArrayIndexOutOfBoundsException e) {
							// e.printStackTrace();
						}
					}

					bfr.close();
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}

			}
			logger.log(Level.INFO, "Total entries in Transliteration Map for IL-Hindi:"
					+ String.valueOf(TRANSLITERATION_DICTIONARY_MAP_HI.size()));

		}
		queryTranslationService = QueryTranslationService.getInstance();
	}

	/**
	 * @param language
	 *            The source language in 2 character codes e.g. 'en','hi'
	 * @param searchElement
	 *            The String to be converted
	 * @return A Map of (String,String) consisting of either both Hindi and English
	 *         conversion of searchElement or only English conversion of
	 *         searchElement (if language is 'hi')
	 * @deprecated
	 */
	public HashMap<String, String> getCrossLingualMapFromSearchQuery(String language, String searchElement)
			throws Exception {
		input = this.getClass().getClassLoader().getResourceAsStream("serverconfig.properties");
		if (input != null) {
			prop.load(input);
		} else {
			throw new FileNotFoundException("property file '" + input + "' not found in the classpath");
		}
		searchElement = searchElement.trim();

		if (language_supported == null || language_supported.isEmpty()) {
			String[] spiltLanguages = prop.get("LANGUAGES_SUPPORTED").toString().split(",");
			language_supported = new ArrayList<>();
			language_supported = Arrays.asList(spiltLanguages);
		}
		if (!language_supported.contains(language)) {
			return null;
		}

		boolean flagForEnablingEnglishLanguage = true;
		boolean flagForEnablingHindiLanguage = true;

		boolean flagForEnglishTranslationCompleted = false;
		boolean flagForHindiTranslationCompleted = false;

		String searchElementTranslatedEnglish = "";
		String searchElementTranslatedHindi = "";

		HashMap<String, String> searchElementEnglishTranslationMap = new HashMap<>();
		HashMap<String, String> searchElementHindiTranslationMap = new HashMap<>();
		String[] searchElementSpilt = searchElement.split(" ");

		HashMap<String, String> translatedSearchElement = new HashMap<>();
		translatedSearchElement.put(language, searchElement);
		translatedSearchElement.put("en", "");
		if (language.equals("hi")) {
			flagForEnablingHindiLanguage = false;
		} else {
			translatedSearchElement.put("hi", "");

		}

		for (int i = 0; i < searchElementSpilt.length; i++) {
			if (!searchElementSpilt[i].trim().equals("")) {
				searchElementEnglishTranslationMap.put(searchElementSpilt[i], "");
				if (flagForEnablingHindiLanguage)
					searchElementHindiTranslationMap.put(searchElementSpilt[i], "");
			}
		}
		logger.log(Level.INFO, "Query: " + searchElement + " language: " + language);

		// NE lookup
		logger.log(Level.INFO, "Going for NE Lookup");
		if (flagForEnablingEnglishLanguage && !flagForEnglishTranslationCompleted) {
			try {
				if (!NE_DICTIONARY_MAP_EN.isEmpty()) {
					{
						if (NE_DICTIONARY_MAP_EN.containsKey(searchElement)) {

							searchElementTranslatedEnglish = NE_DICTIONARY_MAP_EN.get(searchElement);
							System.out.println("Direct English result found in NE Lookup English:"
									+ searchElementTranslatedEnglish);
							flagForEnglishTranslationCompleted = true;
						} else {
							for (int i = 0; i < searchElementSpilt.length; i++) {
								if (NE_DICTIONARY_MAP_EN.containsKey(searchElementSpilt[i])) {
									searchElementEnglishTranslationMap.put(searchElementSpilt[i],
											NE_DICTIONARY_MAP_EN.get(searchElementSpilt[i]));
									System.out.println("for " + searchElementSpilt[i] + " english is: "
											+ NE_DICTIONARY_MAP_EN.get(searchElementSpilt[i]));
								}
							}
						}
					}
				} else {
					throw new Exception("NE Dictionary Map for English is not Intialized -- skipping Engish NE lookup");

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (flagForEnablingHindiLanguage && !flagForHindiTranslationCompleted) {
			try {
				if (!NE_DICTIONARY_MAP_HI.isEmpty()) {
					{
						if (NE_DICTIONARY_MAP_HI.containsKey(searchElement)) {

							searchElementTranslatedHindi = NE_DICTIONARY_MAP_HI.get(searchElement);
							System.out.println(
									"Direct Hindi result found in NE Lookup Hindi:" + searchElementTranslatedHindi);
							flagForHindiTranslationCompleted = true;
						} else {
							for (int i = 0; i < searchElementSpilt.length; i++) {
								if (NE_DICTIONARY_MAP_HI.containsKey(searchElementSpilt[i])) {
									searchElementHindiTranslationMap.put(searchElementSpilt[i],
											NE_DICTIONARY_MAP_HI.get(searchElementSpilt[i]));
									System.out.println("for " + searchElementSpilt[i] + " hindi is: "
											+ NE_DICTIONARY_MAP_HI.get(searchElementSpilt[i]));
								}
							}
						}
					}
				} else {
					throw new Exception("NE Dictionary Map for Hindi is not Intialized -- skipping Hindi NE lookup");

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		boolean flag1 = false;
		if (flagForEnablingEnglishLanguage && !flagForEnglishTranslationCompleted) {
			for (int i = 0; i < searchElementSpilt.length; i++) {
				System.out.println("here :" + searchElementEnglishTranslationMap.get(searchElementSpilt[i]));
				if (searchElementEnglishTranslationMap.get(searchElementSpilt[i]).equals("")) {
					flag1 = true;
					break;

				}
			}

		}

		if (flag1 == false)
			flagForEnglishTranslationCompleted = true;
		flag1 = false;
		if (flagForEnablingHindiLanguage && !flagForHindiTranslationCompleted) {
			for (int i = 0; i < searchElementSpilt.length; i++) {
				if (searchElementHindiTranslationMap.get(searchElementSpilt[i]).equals("")) {
					flag1 = true;
					break;
				}
			}

		}
		if (flag1 == false)
			flagForHindiTranslationCompleted = true;
		// NE lookup ends
		// Translation, transliteration & model lookup
		if (flagForEnablingEnglishLanguage && !flagForEnglishTranslationCompleted) {

			for (int i = 0; i < searchElementSpilt.length; i++) {
				if (searchElementEnglishTranslationMap.get(searchElementSpilt[i]).equals("")) {
					try {
						if (!TRANSLATION_DICTIONARY_MAP_EN.isEmpty()
								&& TRANSLATION_DICTIONARY_MAP_EN.containsKey(searchElementSpilt[i])) {
							searchElementEnglishTranslationMap.put(searchElementSpilt[i],
									TRANSLATION_DICTIONARY_MAP_EN.get(searchElementSpilt[i]));
							System.out.println("Translation Dictionary for English called for " + searchElementSpilt[i]
									+ " found " + TRANSLATION_DICTIONARY_MAP_EN.get(searchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						if (!TRANSLITERATION_DICTIONARY_MAP_EN.isEmpty()
								&& TRANSLITERATION_DICTIONARY_MAP_EN.containsKey(searchElementSpilt[i])) {
							searchElementEnglishTranslationMap.put(searchElementSpilt[i],
									TRANSLITERATION_DICTIONARY_MAP_EN.get(searchElementSpilt[i]));
							System.out.println(
									"Transliteration Dictionary for English called for " + searchElementSpilt[i]
											+ " found " + TRANSLITERATION_DICTIONARY_MAP_EN.get(searchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						if (queryTranslationService != null) {
							searchElementEnglishTranslationMap.put(searchElementSpilt[i],
									queryTranslationService.RPCCall(language, "en", searchElementSpilt[i]));
							System.out.println(
									"Translation service for English called for " + searchElementSpilt[i] + " found "
											+ queryTranslationService.RPCCall(language, "en", searchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}
		if (flagForEnablingHindiLanguage && !flagForHindiTranslationCompleted) {

			for (int i = 0; i < searchElementSpilt.length; i++) {
				if (searchElementHindiTranslationMap.get(searchElementSpilt[i]).equals("")) {
					try {
						if (!TRANSLATION_DICTIONARY_MAP_HI.isEmpty()
								&& TRANSLATION_DICTIONARY_MAP_HI.containsKey(searchElementSpilt[i])) {
							searchElementHindiTranslationMap.put(searchElementSpilt[i],
									TRANSLATION_DICTIONARY_MAP_HI.get(searchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						if (!TRANSLITERATION_DICTIONARY_MAP_HI.isEmpty()
								&& TRANSLITERATION_DICTIONARY_MAP_HI.containsKey(searchElementSpilt[i])) {
							searchElementHindiTranslationMap.put(searchElementSpilt[i],
									TRANSLITERATION_DICTIONARY_MAP_HI.get(searchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						if (queryTranslationService != null) {
							searchElementHindiTranslationMap.put(searchElementSpilt[i],
									queryTranslationService.RPCCall(language, "hi", searchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}

		// Translation and transliteration Dictionary lookup ends

		// if NE lookup succeded

		if (flagForEnablingEnglishLanguage && !searchElementTranslatedEnglish.equals(""))
			translatedSearchElement.put("en", searchElementTranslatedEnglish);
		if (flagForEnablingHindiLanguage && !searchElementTranslatedHindi.equals(""))
			translatedSearchElement.put("hi", searchElementTranslatedHindi);

		// if NE lookup didnt succeded

		if (flagForEnablingEnglishLanguage && searchElementTranslatedEnglish.equals("")) {
			for (int i = 0; i < searchElementSpilt.length; i++) {
				searchElementTranslatedEnglish += searchElementEnglishTranslationMap.get(searchElementSpilt[i]).trim()
						+ " ";
			}
			flagForEnglishTranslationCompleted = true;
		}
		if (flagForEnablingHindiLanguage && searchElementTranslatedHindi.equals("")) {
			for (int i = 0; i < searchElementSpilt.length; i++) {
				searchElementTranslatedHindi += searchElementHindiTranslationMap.get(searchElementSpilt[i]).trim()
						+ " ";
			}
			flagForHindiTranslationCompleted = true;
		}
		if (flagForEnablingEnglishLanguage && flagForEnglishTranslationCompleted) {
			translatedSearchElement.put("en", searchElementTranslatedEnglish);
		}
		if (flagForEnablingHindiLanguage && flagForHindiTranslationCompleted) {
			translatedSearchElement.put("hi", searchElementTranslatedHindi);
		}

		return translatedSearchElement;

	}

	/**
	 * @deprecated
	 * @param sourcelanguage
	 * @param targetLanguage
	 * @param searchElement
	 * @return
	 * @throws Exception
	 * 
	 */
	public HashMap<String, String> getInterCrossLingualMapFromSearchQuery(String sourcelanguage, String targetLanguage,
			String searchElement) throws Exception {
		// We will use Hindi as a midpoiint for N to N language
		// translation-transliteration
		// Basic is convert IL1 query to Hindi language query using
		// translation-transliteration lookup and model, then convert the hindi
		// query to IL2 using the model

		input = this.getClass().getClassLoader().getResourceAsStream("serverconfig.properties");
		if (input != null) {
			prop.load(input);
		} else {
			throw new FileNotFoundException("property file '" + input + "' not found in the classpath");
		}

		if (language_supported == null || language_supported.isEmpty()) {
			String[] spiltLanguages = prop.get("LANGUAGES_SUPPORTED").toString().split(",");
			language_supported = new ArrayList<>();
			language_supported = Arrays.asList(spiltLanguages);
		}
		if (!language_supported.contains(sourcelanguage)) {
			return null;
		}
		HashMap<String, String> translatedSearchElement = new HashMap<>();
		/*
		 * As per requirement translatedSearchElement.put(sourcelanguage,
		 * searchElement);
		 */
		translatedSearchElement.put(targetLanguage, "");

		boolean flagForHindiTranslationCompleted = false;
		boolean flagForIL2ConversionCompleted = false;
		String searchElementInHindi = "";
		String searchElementInIl2 = "";
		String[] searchElementSpilt = searchElement.split(" ");

		HashMap<String, String> searchElementHindiTranslationMap = new HashMap<>();
		HashMap<String, String> hindiConvertedIL2TranslationMap = new HashMap<>();

		for (int i = 0; i < searchElementSpilt.length; i++) {
			searchElementHindiTranslationMap.put(searchElementSpilt[i], "");
		}

		// Part 1 IL1 to Hindi conversion
		// check whtr source langauge is Hindi, if yes, searchElementInHindi to
		// it and set flagForHindiTranslationCompleted as true
		if (sourcelanguage.equals("hi")) {
			searchElementInHindi = searchElement;
			flagForHindiTranslationCompleted = true;
		}
		// NE lookup
		if (!flagForHindiTranslationCompleted) {
			try {
				if (!NE_DICTIONARY_MAP_HI.isEmpty()) {
					{
						if (NE_DICTIONARY_MAP_HI.containsKey(searchElement)) {

							searchElementInHindi = NE_DICTIONARY_MAP_HI.get(searchElement);
							flagForHindiTranslationCompleted = true;
						} else {
							for (int i = 0; i < searchElementSpilt.length; i++) {
								if (NE_DICTIONARY_MAP_HI.containsKey(searchElementSpilt[i])) {
									searchElementHindiTranslationMap.put(searchElementSpilt[i],
											NE_DICTIONARY_MAP_HI.get(searchElementSpilt[i]));
								}
							}
						}
					}
				} else {
					throw new Exception("NE Dictionary Map for Hindi is not Intialized -- skipping Hindi NE lookup");

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		boolean flag1 = false;
		if (!flagForHindiTranslationCompleted) {
			for (int i = 0; i < searchElementSpilt.length; i++) {
				if (searchElementHindiTranslationMap.get(searchElementSpilt[i]).equals("")) {
					flag1 = true;
					break;
				}
			}
			if (flag1 == false)
				flagForHindiTranslationCompleted = true;
		}
		// NE lookup ends
		// Translation - Transliteration lookup and model
		if (!flagForHindiTranslationCompleted) {

			for (int i = 0; i < searchElementSpilt.length; i++) {
				if (searchElementHindiTranslationMap.get(searchElementSpilt[i]).equals("")) {
					try {
						if (!TRANSLATION_DICTIONARY_MAP_HI.isEmpty()
								&& TRANSLATION_DICTIONARY_MAP_HI.containsKey(searchElementSpilt[i])) {
							searchElementHindiTranslationMap.put(searchElementSpilt[i],
									TRANSLATION_DICTIONARY_MAP_HI.get(searchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						if (!TRANSLITERATION_DICTIONARY_MAP_HI.isEmpty()
								&& TRANSLITERATION_DICTIONARY_MAP_HI.containsKey(searchElementSpilt[i])) {
							searchElementHindiTranslationMap.put(searchElementSpilt[i],
									TRANSLITERATION_DICTIONARY_MAP_HI.get(searchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						if (queryTranslationService != null) {
							searchElementHindiTranslationMap.put(searchElementSpilt[i],
									queryTranslationService.RPCCall(sourcelanguage, "hi", searchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}
		if (!flagForHindiTranslationCompleted) {
			for (int i = 0; i < searchElementSpilt.length; i++) {
				searchElementInHindi += searchElementHindiTranslationMap.get(searchElementSpilt[i]).trim() + " ";
			}
			flagForHindiTranslationCompleted = true;

		}
		// Translation - Transliteration lookup and model ends

		// Part 2 Hindi to IL2 conversion
		// check whtr IL2 is Hindi or not, if yes set the searchElementInIl2 to
		// searchElementInHindi and flagForIL2ConversionCompleted to true
		if (targetLanguage.equals("hi")) {
			searchElementInIl2 = searchElementInHindi;
			flagForIL2ConversionCompleted = true;
		}
		// Translation - Transliteration model

		if (flagForHindiTranslationCompleted && !flagForIL2ConversionCompleted) {
			String[] hindiSearchElementSpilt = searchElementInHindi.split(" ");
			for (int i = 0; i < hindiSearchElementSpilt.length; i++) {
				hindiConvertedIL2TranslationMap.put(hindiSearchElementSpilt[i], "");
			}
			for (int i = 0; i < hindiSearchElementSpilt.length; i++) {
				if (hindiConvertedIL2TranslationMap.get(hindiSearchElementSpilt[i]).equals("")) {
					try {
						if (queryTranslationService != null) {
							hindiConvertedIL2TranslationMap.put(hindiSearchElementSpilt[i],
									queryTranslationService.RPCCall("hi", targetLanguage, hindiSearchElementSpilt[i]));
							continue;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			if (!flagForIL2ConversionCompleted) {
				for (int i = 0; i < hindiSearchElementSpilt.length; i++) {
					searchElementInIl2 += hindiConvertedIL2TranslationMap.get(hindiSearchElementSpilt[i]).trim() + " ";
				}
				flagForIL2ConversionCompleted = true;
			}
		}

		if (flagForHindiTranslationCompleted && flagForIL2ConversionCompleted)
			translatedSearchElement.put(targetLanguage, searchElementInIl2.trim());
		// Translation - Transliteration model ends
		return translatedSearchElement;

	}

	/**
	 * @param dateformat
	 * @return
	 * @throws ParseException
	 */
	public String getDate(String dateformat) throws ParseException {

		SimpleDateFormat oldSdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH); // Tue
																											// May
																											// 02
																											// 10:31:00
																											// IST
																											// 2017

		SimpleDateFormat newSdf1 = new SimpleDateFormat("MMM dd, yyyy");
		SimpleDateFormat newSdf2 = new SimpleDateFormat("MMM dd");
		Calendar calendar = Calendar.getInstance();
		Date olddate = (Date) oldSdf.parse(dateformat);
		calendar.setTime(olddate);
		if (calendar.get(Calendar.YEAR) < 1000) {
			return newSdf2.format(olddate);
		} else {
			return newSdf1.format(olddate);
		}
	}

	/**
	 * @param dateformat
	 * @return
	 * @throws ParseException
	 */
	public String getDate(Date olddate) throws ParseException {

		SimpleDateFormat newSdf1 = new SimpleDateFormat("MMM dd, yyyy");
		SimpleDateFormat newSdf2 = new SimpleDateFormat("MMM dd");
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(olddate);
		if (calendar.get(Calendar.YEAR) < 1000) {
			return newSdf2.format(olddate);
		} else {
			return newSdf1.format(olddate);
		}
	}

	/**
	 * @param otherterm
	 * @return
	 */
	public String findSimilar(String otherterm) {
		String indexterm = otherterm;
		switch (otherterm) {
		case "hostname":
			indexterm = "hostname";
			break;
		case "wikicode":
			indexterm = "idcode";
			break;
		case "languages":
			indexterm = "lang";
			break;
		}
		return indexterm;
	}

	/**
	 * @param vasCodeFromUI
	 * @return
	 */
	public String findVasCode(String vasCodeFromUI) {
		String indexterm = "";
		switch (vasCodeFromUI) {
		case "Conferences":
			indexterm = "VA_CON";
			break;
		case "Awards":
			indexterm = "VA_AWD";
			break;
		case "Seminars":
			indexterm = "VA_SEM";
			break;
		case "Workshops":
			indexterm = "VA_WOR";
			break;
		}
		return indexterm;
	}

	/**
	 * @param vasCodeFromSolr
	 * @return
	 */
	public String findVasValue(String vasCodeFromSolr) {
		String indexterm = "";
		switch (vasCodeFromSolr) {
		case "VA_CON":
			indexterm = "Conferences";
			break;
		case "VA_AWD":
			indexterm = "Awards";
			break;
		case "VA_SEM":
			indexterm = "Seminars";
			break;
		case "VA_WOR":
			indexterm = "Workshops";
			break;
		}
		return indexterm;
	}

	/**
	 * @param mapoffilter
	 * @return
	 */
	public boolean extractFilterMapAndAddInQuery(Map<String, List<String>> mapoffilter) {

		for (Map.Entry<String, List<String>> entrySet : mapoffilter.entrySet()) {

			String filterField = entrySet.getKey();

			List<String> filterfieldvaluelist = entrySet.getValue();

			if (filterField.equalsIgnoreCase("wikicode")) {

				List<String> wikicodes = entrySet.getValue();
				for (String wikicode : wikicodes) {
					// wikicode = wikicode;
					logger.log(Level.INFO, "Checking wikicode:" + wikicode);
					this.addFilterQuery("idcode", wikicode);
				}
			} else if (filterField.equalsIgnoreCase("vascode")) {

				List<String> vascodes = entrySet.getValue();
				for (String vascode : vascodes) {

					vascode = findVasCode(vascode);
					logger.log(Level.INFO, "Checking vascode:" + vascode);
					this.addFilterQuery("idcode", vascode); /* 29 june 2017 idcode to idcode */
				}
			} else
				for (String filterfieldvalue : filterfieldvaluelist) {
					logger.log(Level.INFO, "for filterField " + filterField + ", findsimilar()-->"
							+ findSimilar(filterField) + " and value added-->" + filterfieldvalue);

					this.addFilterQuery(findSimilar(filterField), filterfieldvalue);
				}
		}
		return false;

	}

	/**
	 * @param subResourceIdentifierList
	 * @param resourceType
	 */
	public void extractSubResourceIdentifierListAndAddInQuery(List<String> subResourceIdentifierList,
			String resourceType) {

		System.out.println("sub identifier list is" + subResourceIdentifierList.toString());
		StringBuffer stringOfSubResourceIdentifier = new StringBuffer("(");

		switch (resourceType) {
		/*
		 * 29 june 2017 case "VALUE": for (int i = 0; i <
		 * subResourceIdentifierList.size(); i++) { if (i == 0)
		 * stringOfSubResourceIdentifier.append(subResourceIdentifierList.get(i) ); else
		 * stringOfSubResourceIdentifier.append(" OR " +
		 * subResourceIdentifierList.get(i)); }
		 * stringOfSubResourceIdentifier.append(")"); this.addFilterQuery("idcode",
		 * stringOfSubResourceIdentifier.toString()); break;
		 */
		case "WIKI":
			for (int i = 0; i < subResourceIdentifierList.size(); i++) {
				if (i == 0)
					stringOfSubResourceIdentifier.append(subResourceIdentifierList.get(i));
				else
					stringOfSubResourceIdentifier.append(" OR " + subResourceIdentifierList.get(i));
			}
			stringOfSubResourceIdentifier.append(")");
			this.addFilterQuery("idcode", stringOfSubResourceIdentifier.toString().toLowerCase());
			break;

		default: // for enews and govw
			for (int i = 0; i < subResourceIdentifierList.size(); i++) {
				if (i == 0)
					stringOfSubResourceIdentifier.append(subResourceIdentifierList.get(i));
				else
					stringOfSubResourceIdentifier.append(" OR " + subResourceIdentifierList.get(i));
			}
			stringOfSubResourceIdentifier.append(")");
			this.addFilterQuery("idcode", stringOfSubResourceIdentifier.toString().toUpperCase());
			break;

		}

	}

	/**
	 * @param resourceType
	 * @return
	 * @throws IOException
	 */
	public HttpSolrServer getSolrServer(String resourceType) throws IOException {
		String collection = "";
		String port = "";

		logger.log(Level.INFO, "getting SolrServer");
		input = getClass().getClassLoader().getResourceAsStream("solrconfig.properties");
		if (input != null) {
			prop.load(input);
		} else {
			throw new FileNotFoundException("property file '" + input + "' not found in the classpath");
		}
		// for extracting lang from filterMap of searchForm

		String server = prop.getProperty("SOLR_SERVER");
		if (resourceType.equalsIgnoreCase("ENEW")) {
			collection = prop.getProperty("SOLR_COLLECTION_ENEWS_WEBSITES");
			port = prop.getProperty("SOLR_PORT");
			logger.log(Level.INFO, "Server " + server + ":" + port + collection);
		}
		if (resourceType.equalsIgnoreCase("GOVW")) {
			//server = prop.getProperty("SOLR_SERVER_6_POINT_O"); //changes for new govw crawl 28 sep 2017
			//port = prop.getProperty("SOLR_PORT_6_POINT_O");
			//collection = prop.getProperty("SOLR_COLLECTION_GOVT_WEBSITES_6_POINT_O");
			port = prop.getProperty("SOLR_PORT");
			collection = prop.getProperty("SOLR_COLLECTION_GOVT_WEBSITES");
			
		}
		if (resourceType.equalsIgnoreCase("WIKI")) {
			collection = prop.getProperty("SOLR_COLLECTION_WIKI");
			port = prop.getProperty("SOLR_PORT_WIKI");
		}
		if (resourceType.equalsIgnoreCase("VALUE")) {
			// server = prop.getProperty("SOLR_SERVER_VAS");
			collection = prop.getProperty("SOLR_COLLECTION_VAS");
			port = prop.getProperty("SOLR_PORT_VAS");
		}

		//String urlForSolrResponse = "http://" + server + ":" + port + "/solr/" + collection + "/";
		String urlForSolrResponse = "http://" + server + ":" + port + "/solr/" + collection;
		logger.log(Level.INFO, "server url is " + urlForSolrResponse);

		boolean isAlive = true;
		try {
			// Checking whtr the Solr is responsive or not
			URL url = new URL(urlForSolrResponse);
			HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.connect();
			httpURLConnection.disconnect();
		} catch (Exception e) {
			logger.log(Level.INFO, "Server " + server + ":" + port + " Not alive--Shifting for backup server");
			server = prop.getProperty("SOLR_SERVER_BACKUP");
			port = prop.getProperty("SOLR_PORT_BACKUP");

			if (resourceType.equalsIgnoreCase("ENEW"))
				collection = prop.getProperty("SOLR_COLLECTION_ENEWS_WEBSITES_BACKUP");
			if (resourceType.equalsIgnoreCase("GOVW"))
				collection = prop.getProperty("SOLR_COLLECTION_GOVT_WEBSITES_BACKUP");
			if (resourceType.equalsIgnoreCase("WIKI")) {
				collection = prop.getProperty("SOLR_COLLECTION_WIKIPEDIA_BACKUP");
				port = prop.getProperty("SOLR_PORT_WIKI");
			}
			if (resourceType.equalsIgnoreCase("VALUE")) {
				collection = prop.getProperty("SOLR_COLLECTION_VAS_BACKUP");
				port = prop.getProperty("SOLR_PORT_VAS");
			}

			urlForSolrResponse = "http://" + server + ":" + port + "/solr/" + collection;
			isAlive = true;
			try {
				// Checking whtr the Solr is responsive or not
				URL url = new URL(urlForSolrResponse);
				HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
				httpURLConnection.connect();
				httpURLConnection.disconnect();
			} catch (Exception e1) {
				logger.log(Level.INFO, "Backup Server " + server + ":" + port + " Not alive--Search Failed");
				isAlive = false;

			}
			httpSolrServer = new HttpSolrServer(urlForSolrResponse);
		}

		try {
			// using solr API

			httpSolrServer = new HttpSolrServer(urlForSolrResponse);
			logger.log(Level.INFO, "Solr Server acknowledge, going for search!");

		} catch (Exception e) {
			httpSolrServer = null;
			e.printStackTrace();
		}

		return httpSolrServer;
	}

	/**
	 * @param String[]
	 *            one
	 * @param String[]
	 *            two
	 * @return
	 */
	public static String[] cartesianProduct(String[] one, String[] two) {
		int len;
		if (one.length != 0 && two.length != 0) {
			len = one.length * two.length;
		} else if (one.length == 0)
			len = two.length;
		else
			len = one.length;
		String[] result = new String[len];
		int index = 0;

		for (String v1 : one) {
			for (String v2 : two) {
				result[index] = v1 + " " + v2;
				index++;
			}
		}

		return result;
	}

	/**
	 * 
	 */
	public static void createDidYouMeanDirectory() {
		try {
			sp = new SpellChecker(FSDirectory
					.open(new File(ProcessingDao.class.getClassLoader().getResource("resources") + "/fsdirectory")));
			// index the dictionary
			sp.indexDictionary(new PlainTextDictionary(
					ProcessingDao.class.getClassLoader().getResourceAsStream("english_dictionary.txt")));
			sp.indexDictionary(new PlainTextDictionary(
					ProcessingDao.class.getClassLoader().getResourceAsStream("hindi_dictionary.txt")));
			System.out
					.println("---xx---Creating \"Did You Mean Directory\" for English & Hindi Language---xx---Success");
		} catch (Exception e) {
			System.out
					.println("---xx---Creating \"Did You Mean Directory\" for English & Hindi Language---xx---Failed\n"
							+ e.getMessage());
		}
	}

	public void createSpellCheckerDictionary(String path) {
		try {
			if (!path.equals("")) {
				System.out.println("Path:" + path);
				if (sc == null) {
					sc = new ToySpellingCorrector();
					System.out.println("Here for creating spell checker dictionary");
					sc.trainFile(path);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param filterqueryparameter
	 * @param filterqueryparametervalue
	 * @return
	 */
	public boolean addFilterQuery(String filterqueryparameter, String filterqueryparametervalue) {

		if (filterqueryparameter != null && filterqueryparameter.length() != 0 && filterqueryparametervalue != null
				&& filterqueryparametervalue.length() != 0) {
			logger.log(Level.INFO, "FilterQParam:" + filterqueryparameter + "  value:" + filterqueryparametervalue);
			solrquery.addFilterQuery(filterqueryparameter + ":" + filterqueryparametervalue);
			return true;
		} else
			return false;

	}

	/**
	 * @param Facetfield
	 * @return
	 */
	public boolean addFacetField(String Facetfield) {
		solrquery.setFacet(true);
		if (Facetfield != null || Facetfield.length() != 0) {

			solrquery.addFacetField(Facetfield);
			solrquery.set("facet.limit", -1);
			return true;
		} else
			return false;

	}

	/**
	 * @param userQuery
	 * @return
	 */
	public boolean queryIntialization(String userQuery, String resourceType) {
		try {
			logger.log(Level.INFO, "query initialized");
			if (userQuery != null && !userQuery.equals("")) {

				solrquery = new SolrQuery(userQuery);
				solrquery.setStart(0);
				solrquery.setRows(10);
				
				this.addFilterQuery("idcode", "(NOT NOTFND)");
				this.addFilterQuery("boost", "(NOT 0)");
				this.addFilterQuery("content", "(NOT #HUBPAGE)");
				this.addFilterQuery("resourceType", resourceType);
				/* solrquery.set("wt","json"); */
				return true;
			} else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * @param userQuery
	 * @param filterMap
	 * @param pageNum
	 * @param pageWin
	 * @param resourceTypeRecieved
	 * @param facetfields
	 * @return
	 */
	public boolean queryIntialization(String userQuery, Map<String, List<String>> filterMap, String pageNum,
			String pageWin, String resourceTypeRecieved, List<String> facetfields, String requestLanguage) {
		try {

			if (userQuery != null && !userQuery.equals("")) {
				int userQueryLength = userQuery.split(" ").length;
				solrquery = new SolrQuery();
				this.addFilterQuery("resourceType", resourceTypeRecieved);
				if (!pageNum.equalsIgnoreCase("") && !pageWin.equalsIgnoreCase("")) {
					int start = (Integer.valueOf(pageNum) - 1) * Integer.valueOf(pageWin);
					solrquery.setStart(start);
					solrquery.setRows(Integer.valueOf(pageWin));

				} else {
					// deaultparams
					solrquery.setStart(0);
					solrquery.setRows(10);

				}

				// Add facet fields
				for (String facetfield : facetfields)
					this.addFacetField(facetfield);

				// Query Processing 29 june 2017
				
				switch (resourceTypeRecieved) {
				
				case "ENEW":
					this.addFilterQuery("idcode", "(NOT NOTFND)");
					this.addFilterQuery("boost", "(NOT 0)");
					this.addFilterQuery("content", "(NOT #HUBPAGE)");
					solrquery.set("facet.limit", -1);
					if (userQuery.equals("*.*")) {
						userQuery = "title:(*:*)^100 content:(*:*)^20";
						solrquery.setHighlight(false);
						solrquery.set("bf", "recip(ms(NOW,articleDate),3.16e-11,1,1)");
						System.out.println("Adding language as a filter for default query ENEW");/*added by pragya on 6th oct 2017 for localization*/
						/* Removed on request of HCDC -- 01-November 2017 */
						//this.addFilterQuery("lang",requestLanguage);
					} else {
						/*changed on 17 Nov as per new requirement of phrase queries adding duoble quotes to query in bracket*/
						userQuery = "(\"" + userQuery + "\")^1000 OR " + userQuery;
						/*commneted on 17 Nov as per new requirement of phrase queries
						solrquery.set("defType", "edismax");
						solrquery.set("qf", "title^100 content^20");
						solrquery.set("bf","recip(ms(NOW,articleDate),3.16e-11,1,1)");
						solrquery.set("df", "title,content");*/
						/*
						 * changes for exact match as discussed with DK on 23rd Oct 2017
						 * solrquery.set("mm", userQueryLength - 1); 
						*/
						//solrquery.set("mm", userQueryLength);
						//till here
						solrquery.setHighlight(true);
						solrquery.setHighlightSimplePost(HIGHLIGHTER_CSS_END_TAG);
						solrquery.setHighlightSimplePre(HIGHLIGHTER_CSS_START_TAG);
						solrquery.set("hl.usePhraseHighlighter", true);
						solrquery.addHighlightField("content");
						solrquery.addHighlightField("title");
						solrquery.setHighlightFragsize(200);
						solrquery.setHighlightSnippets(3);
						
					}

					solrquery.setQuery(userQuery);
					
					break;
				case "GOVW":
					solrquery.set("facet.limit", -1);
					if (userQuery.equals("*.*")) {
						userQuery = "title:(*:*)^100 content:(*:*)^20";
						solrquery.setHighlight(false);
						this.addFilterQuery("idcode", "(NOT NOTFND)");
						this.addFilterQuery("boost", "(NOT 0)");
						//this.addFilterQuery("depth", "0");
						this.addFilterQuery("content", "(NOT #HUBPAGE)");
						System.out.println("Adding language as a filter for default query GOVW"); /*added by pragya on 6th oct 2017 for localization*/
						this.addFilterQuery("lang","en");
						solrquery.set("bq", "depth:0"); //added on 27 oct 2017
						//solrquery.set("bf", "recip(ms(NOW,articleDate),3.16e-11,1,1)");
					} else {
						userQuery = "(" + userQuery + ")^100 OR " + userQuery;
						solrquery.setHighlight(true);
						solrquery.setHighlightSimplePost(HIGHLIGHTER_CSS_END_TAG);
						solrquery.setHighlightSimplePre(HIGHLIGHTER_CSS_START_TAG);
						solrquery.set("hl.usePhraseHighlighter", true);
						solrquery.addHighlightField("content");
						solrquery.addHighlightField("title");
						solrquery.setHighlightFragsize(200);
						solrquery.setHighlightSnippets(3);
					}

					solrquery.setQuery(userQuery);
					solrquery.set("defType", "edismax");
					solrquery.set("qf", "title^100 content^20");
					/*
					 * changes for exact match as discussed with DK on 23rd Oct 2017
					 * solrquery.set("mm", userQueryLength - 1); 
					*/
					solrquery.set("mm", userQueryLength);
					// solrquery.set("bf",
					// "recip(ms(NOW,fetchTime),3.16e-11,1,1)");
					solrquery.set("df", "title,content");
					break;
				case "WIKI":
					solrquery.set("facet.limit", -1);
					if (userQuery.equals("*.*")) {
						userQuery = "title:(*:*)^100 content:(*:*)^20";
						solrquery.setHighlight(false);
					} else {
						userQuery = "(" + userQuery + ")^100 OR " + userQuery;
						solrquery.setHighlight(true);
						solrquery.setHighlightSimplePost(HIGHLIGHTER_CSS_END_TAG);
						solrquery.setHighlightSimplePre(HIGHLIGHTER_CSS_START_TAG);
						solrquery.set("hl.usePhraseHighlighter", true);
						solrquery.addHighlightField("content");
						solrquery.addHighlightField("title");
						solrquery.setHighlightFragsize(200);
						solrquery.setHighlightSnippets(3);
					}

					solrquery.setQuery(userQuery);
					solrquery.set("defType", "edismax");
					solrquery.set("qf", "title^100 content^20");
					/*
					 * changes for exact match as discussed with DK on 23rd Oct 2017
					 * solrquery.set("mm", userQueryLength - 1); 
					*/
					solrquery.set("mm", userQueryLength);
					solrquery.set("df", "title,content");
					break;
				case "VALUE":
					solrquery.set("facet.limit", -1);
					if (userQuery.equals("*.*")) {
						userQuery = "title:(*:*)^100 awardRecipientName:(*:*)^80 content:(*:*)^20";
						solrquery.setHighlight(false);
					} else {
						userQuery = userQuery; /*
												 * In testing phase for relevancy will change later
												 */
						solrquery.setHighlight(true);
						solrquery.setHighlightSimplePost(HIGHLIGHTER_CSS_END_TAG);
						solrquery.setHighlightSimplePre(HIGHLIGHTER_CSS_START_TAG);
						solrquery.set("hl.usePhraseHighlighter", true);
						solrquery.addHighlightField("content");
						solrquery.addHighlightField("title");
						solrquery.addHighlightField("awardRecipientName");
						solrquery.addHighlightField("awardField");
						solrquery.addHighlightField("location");
						solrquery.setHighlightFragsize(200);
						solrquery.setHighlightSnippets(3);
					}

					solrquery.setQuery(userQuery);
					solrquery.set("defType", "edismax");
					solrquery.set("qf", "title^100 awardRecipientName^80 content^20");
					/*
					 * changes for exact match as discussed with DK on 23rd Oct 2017
					 * solrquery.set("mm", userQueryLength - 1); 
					*/
					solrquery.set("mm", userQueryLength);
					solrquery.set("bf", "recip(ms(NOW,date),3.16e-11,1,1)");
					solrquery.set("df", "title,content,awardRecipientName,awardField,location");
					break;

				default:
					if (userQuery.equals("*.*")) {
						userQuery = "title:(*:*)^100 content:(*:*)^20";
						solrquery.setHighlight(false);
					} else {
						userQuery = "(" + userQuery + ")^100 OR " + userQuery;
						solrquery.setHighlight(true);
						solrquery.setHighlightSimplePost(HIGHLIGHTER_CSS_END_TAG);
						solrquery.setHighlightSimplePre(HIGHLIGHTER_CSS_START_TAG);
						solrquery.set("hl.usePhraseHighlighter", true);
						solrquery.addHighlightField("content");
						solrquery.addHighlightField("title");
						solrquery.setHighlightFragsize(200);
						solrquery.setHighlightSnippets(3);
					}

					solrquery.setQuery(userQuery);
					solrquery.set("defType", "edismax");
					solrquery.set("qf", "title^100 content^20");
					solrquery.set("pf", "content");
					solrquery.set("ps", "0");
					/*
					 * changes for exact match as discussed with DK on 23rd Oct 2017
					 * solrquery.set("mm", userQueryLength - 1); 
					*/
					solrquery.set("mm", userQueryLength);
					// solrquery.set("bf",
					// "recip(ms(NOW,articleDate),3.16e-11,1,1)");
					solrquery.set("df", "title,content");
					break;
				}

				return true;
			} else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * @return QueryResponse object
	 */
	/**
	 * @return
	 */
	public QueryResponse queryResponse() {

		queryResponse = new QueryResponse();
		try {
			logger.log(Level.INFO, "Query passed to solr:" + solrquery.getQuery());
			logger.log(Level.INFO, "Query Params to solr: ");
			if (solrquery != null && httpSolrServer != null) {
				queryResponse = httpSolrServer.query(solrquery);
			}

		} catch (SolrServerException e) {
			e.printStackTrace();

		}
		return queryResponse;

	}

	/**
	 * Generate a cleaned query for better searching
	 * 
	 * @param query
	 * @return Cleaned query String
	 */
	public String cleanQuery(String query) {
		query = query.trim();
		if (query.contains("\""))
			query = query.replaceAll("\"", "");
		return query;
	}

	public static void main(String[] args) {
		ProcessingDao build = new ProcessingDao();
		try {
			System.out.println(build.getCrossLingualMapFromSearchQuery("te", "కొత్త సంవత్సరం మొదటి రోజు"));
			System.out.println(build.getInterCrossLingualMapFromSearchQuery("te", "pa", "కొత్త సంవత్సరం మొదటి రోజు"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}