package cdac.nvli.util.service;

import java.util.HashMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class QueryTranslationService {
	private static QueryTranslationService qtEngine = null;
	// contains the port of all the language transliteration models hosted on
	// the server.
	private HashMap<String, String> PORTS = new HashMap<String, String>();
	private static HashMap<String, XmlRpcClient> CLIENTS;

	// args1 = source language
	// args2 = target language
	// args3 = TextToTranslate
	public static void main(String[] args) {
		QueryTranslationService.getInstance();
		String source = "ta";
		String target = "en";
		String finalout = "";
		// String query = "ਭਾਰਤ ਇਕ ਮਹਾਨ ਦੇਸ਼ ਹੈ";
		// String query = "भारत इक महान देस़ है";
		String query = "வலி ஏற்படுகிறது";
		// String query = "india";
		String[] spiltQuery = query.split(" ");
		for (int k = 0; k < spiltQuery.length; k++) {
			System.out.println(spiltQuery[k]);
			String transltaion = qtEngine.RPCCall(source, target, spiltQuery[k]);
			System.out.println(transltaion);
			String[] spilt = transltaion.replaceAll("|unk", "").replaceAll("unk|", "").split("|");

			for (int i = 0; i < spilt.length; i++) {
				System.out.println(spilt[i]);
				if (!spilt[i].matches("[0-9|-]+"))
					finalout += spilt[i];
			}
			finalout += " ";
		}
		System.out.println("Final out:" + finalout);
	}

	public static QueryTranslationService getInstance() {
		if (qtEngine == null) {
			qtEngine = new QueryTranslationService();
		}
		return qtEngine;
	}

	private QueryTranslationService() {
		CLIENTS = new HashMap<String, XmlRpcClient>();
		if (PORTS.isEmpty()) {
			PORTS = readPorts("ports.txt");
		}
		Object[] ports = PORTS.keySet().toArray();
		for (int i = 0; i < ports.length; i++) {
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			try {
				config.setServerURL(new URL("http://" + PORTS.get((String) ports[i]) + "/RPC2"));
			} catch (Exception E) {
				System.out.println(E.toString());
			}
			XmlRpcClient client = new XmlRpcClient();
			client.setConfig(config);
			CLIENTS.put((String) ports[i], client);
		}
	}

	public String RPCCall(String sourceLanguage, String targetLanguage, String translateText) {
		try {
			/*
			 * if(PORTS.isEmpty()){ PORTS =
			 * readPorts(solrParams.get(DisMaxParams.TRANSLITERATION_MODELS,"")+
			 * "ports.txt"); } String port=PORTS.get(arg1+"-"+arg2); // Create
			 * an instance of XmlRpcClient XmlRpcClientConfigImpl config = new
			 * XmlRpcClientConfigImpl(); config.setServerURL(new
			 * URL("http://"+port+"/RPC2")); XmlRpcClient client = new
			 * XmlRpcClient(); client.setConfig(config);
			 */
			// The XML-RPC data type used by mosesserver is <struct>. In Java,
			// this data type can be represented using HashMap.
			HashMap<String, Object> mosesParams = new HashMap<String, Object>();
			String textToTranslate = split(sourceLanguage, targetLanguage, translateText);
			mosesParams.put("text", textToTranslate);
			mosesParams.put("align", "true");
			// mosesParams.put("nbest", 10);
			mosesParams.put("nbest-distinct", "true");
			mosesParams.put("report-all-factors", "true");
			// The XmlRpcClient.execute method doesn't accept Hashmap (pParams).
			// It's either Object[] or List.
			Object[] params = new Object[] { null };
			params[0] = mosesParams;
			// Invoke the remote method "translate". The result is an Object,
			// convert it to a HashMap.
			// HashMap result = (HashMap)client.execute("translate", params);
			System.out.println(sourceLanguage + "-" + targetLanguage);
			HashMap result = (HashMap) CLIENTS.get(sourceLanguage + "-" + targetLanguage).execute("translate", params);
			// Print the returned results
			// String textTranslation = (String)result.get("text");
			String textTranslation = combine(sourceLanguage, targetLanguage, result);
			// Added code by lovey for cleaning up translated text
			String[] spilt = textTranslation.replaceAll("|unk", "").replaceAll("unk|", "").split("|");
			String finaloutTranslation = "";
			for (int i = 0; i < spilt.length; i++) {
				// System.out.println(spilt[i]);
				if (!spilt[i].matches("[0-9|-]+"))
					finaloutTranslation += spilt[i];
			}
			finaloutTranslation += " ";
			return finaloutTranslation;
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
	}

	protected String split(String sLang, String tLang, String text) {
		char[] charText = text.toCharArray();
		String tokenizedText = new String();
		for (int i = 0; i < charText.length; i++) {
			tokenizedText += " " + charText[i];
		}
		return tokenizedText.trim();
	}

	protected String combine(String sLang, String tLang, HashMap result) {
		if (sLang == "te") {
			return ((String) result.get("text")).replaceAll("\\s+", "").toLowerCase();
		}
		// if(arg1=="be"){
		// combine(result);
		// }
		return ((String) result.get("text")).replaceAll("\\s+", "").toLowerCase();
	}

	private HashMap<String, String> readPorts(String fileName) {
		HashMap<String, String> ports = new HashMap<String, String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(QueryTranslationService.class
					.getClassLoader().getResourceAsStream("QueryTranslationServicePorts.txt"), "UTF-8"));
			String line = reader.readLine();
			String[] tokens = null;
			while (line != null) {
				tokens = line.trim().split("\t");
				if (tokens.length == 2) {
					ports.put(tokens[0].trim().toLowerCase(), tokens[1].trim().toLowerCase());
				}
				line = reader.readLine();
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ports;
	}
}