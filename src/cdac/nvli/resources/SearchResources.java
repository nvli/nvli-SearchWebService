package cdac.nvli.resources;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import cdac.nvli.bean.*;
import cdac.nvli.util.dao.*;
import cdac.nvli.util.solr.*;

/**
 * @author lovey@cdacp
 * @category AAI group RestWebService
 */
@Path("/webservice")
public class SearchResources {

	public final String[] resourceTypeCodes = { "ENEW", "GOVW", "WIKI", "VALUE" };
	ProcessingDao processingDao = new ProcessingDao();

	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	public static Logger logger = Logger.getLogger("SearchResources");
	List<Doc> responseDoc;
	List<SearchResult> searchResult;
	String result;
	FacetNameCountHolder facetNameCountHolder;
	public final static String HIGHLIGHTER_CSS_START_TAG = "<mark>";
	public final static String HIGHLIGHTER_CSS_END_TAG = "</mark>";
	public ArrayList<String> facetfields = new ArrayList<>();
	private QueryResponse queryResponse = new QueryResponse();

	/**
	 * @param s
	 *            as SearchForm
	 * @return
	 */
	private SearchForm initSearchForm(SearchForm s) {
		Long ln = 0L;
		if (s.getFilters() == null) {
			s.setFilters(new HashMap<>());
		}
		if (s.getFacetMap() == null)
			s.setFacetMap(new HashMap<>());
		if (s.getAdvanceSearchQueryString() == null)
			s.setAdvanceSearchQueryString(new String());
		if (s.getAllResources() == null)
			s.setAllResources(new HashMap<>());
		if (s.getFromDate() == null)
			s.setFromDate("");
		if (s.getToDate() == null)
			s.setToDate("");
		if (s.getRankingRule() == null)
			s.setRankingRule("");
		if (s.getResourceCode() == null)
			s.setResourceCode("");
		if (s.getResultSize() == null)
			s.setResultSize(ln);
		if (s.getSearchElement() == null || s.getSearchElement().equals(""))
			s.setSearchElement("*.*");
		if (s.getListOfResult() == null)
			s.setListOfResult(new ArrayList<>());
		if (s.getSpecificFacetMap() == null)
			s.setSpecificFacetMap(new HashMap<>());
		if (s.getFilters() == null)
			s.setFilters(new HashMap<>());
		if (s.getSpecificFilters() == null)
			s.setSpecificFilters(new HashMap<>());
		if (s.getSubResourceIdentifiers() == null)
			s.setSubResourceIdentifiers(new ArrayList<>());

		// Add facetfield in list here

		if (s.getResourceCode().equals("VALUE"))
			facetfields.add("idcode");
		else {
			facetfields.add("lang");
			facetfields.add("hostname");
			facetfields.add("idcode");
		}
		if (s.getResourceCode().equals("ENEW")) // check for GOVW too
			facetfields.add("domain");
		return s;
	}

	/**
	 * @return
	 * @deprecated
	 */
	@POST
	@Path("/basicSearch1")
	@Produces({ MediaType.APPLICATION_JSON })
	public SearchForm getListOfResult() {
		// logger.log(Level.INFO, msg);

		logger.log(Level.INFO, "path encountered: basicSearch1");

		queryResponse = new QueryResponse();
		// String q = "इंडिया";
		String q = "*.*";
		logger.log(Level.INFO, "in basicSearch1 q is..." + q);
		String resourceCode = "GOVW";
		SearchForm searchForm = new SearchForm();
		// Init Searchform for HCDC
		searchForm = initSearchForm(searchForm);
		searchForm.setSearchElement(q);
		searchForm.setResourceCode(resourceCode);
		Map<String, List<String>> filterMap = new HashMap<>();
		if (searchForm.getFilters() != null)
			filterMap = searchForm.getFilters();

		searchResult = new ArrayList<SearchResult>();
		// String f = format != null ? format : "";
		logger.log(Level.INFO, "Got input: q:" + q);// + " s:" + s + " m:" + m +
													// " f:"+ f);
		// String lang = "gu";
		// for extracting lang from filterMap of searchForm
		/*
		 * if (filterMap != null) { for (Map.Entry<String, List<String>> entrySet :
		 * filterMap.entrySet()) { String filterField = entrySet.getKey(); if
		 * (filterField.equalsIgnoreCase("lang")) { List<String> langList =
		 * entrySet.getValue(); for (String langStr : langList) lang =
		 * langStr.toUpperCase(); }
		 * 
		 * }
		 * 
		 * }
		 */
		try {
			if (!processingDao.queryIntialization(q, resourceCode))
				throw new Exception();
			// logger.log(Level.INFO, "in basicSearch1 lang is..." + lang);
			if (!searchForm.getResourceCode().equals("")) {
				processingDao.addFilterQuery("resourceType", resourceCode);
				processingDao.getSolrServer(searchForm.getResourceCode());
			}
			/*
			 * if ((queryResponse=processingDao.queryResponse())!=null) throw new
			 * Exception();
			 */

			/*
			 * processingDao.addFacetField("hostname");
			 * processingDao.addFacetField("baseUrl"); processingDao.addFacetField("lang");
			 */
			queryResponse = processingDao.queryResponse();
			SolrDocumentList solrDocumentList = queryResponse.getResults();

			if (solrDocumentList != null) {

				for (SolrDocument doc : solrDocumentList) {

					SearchResult sr = new SearchResult();
					// Create Objects if required
					List<String> title = new ArrayList<String>();
					List<String> language = new ArrayList<String>();
					List<String> description = new ArrayList<String>();
					List<String> source = new ArrayList<String>();
					List<String> date = new ArrayList<String>();
					// Populate the arraylists of objects if required per doc
					title.add(doc.getFieldValue("title").toString());
					language.add(doc.getFieldValue("lang").toString());
					/*
					 * description.add(doc.getFieldValue("content").toString(). replaceAll("\n",
					 * " ").toLowerCase() .replaceAll("\\b" + q.toLowerCase() + "\\b",
					 * HIGHLIGHTER_CSS_START_TAG + q.toLowerCase() + HIGHLIGHTER_CSS_END_TAG));
					 */

					source.add(doc.getFieldValue("url").toString());
					date.add(processingDao.getDate(doc.getFieldValue("tstamp").toString()));
					// date.add(doc.getFieldValue("date").toString());

					// Set the Objects in Search Result, that are made above, or
					// else set the direct results from JSON object from Solr
					sr.setResourceType(doc.getFieldValue("resourceType").toString());
					sr.setRecordIdentifier(doc.getFieldValue("identifier").toString());
					sr.setTitle(title);
					sr.setLanguage(language);
					sr.setDescription(description);
					sr.setSource(source);
					sr.setData(date);

					/*
					 * try { if (!doc.getFieldValue("title").toString().equals(null) &&
					 * !doc.getFieldValue("title").toString().equals("")) {
					 * 
					 * sr.setNameToView(doc.getFieldValue("title").toString()); } else {
					 * sr.setNameToView(doc.getFieldValue("identifier").toString
					 * ().toLowerCase().replaceAll( "\\b" + q.toLowerCase() + "\\b",
					 * HIGHLIGHTER_CSS_START_TAG + q.toLowerCase() + HIGHLIGHTER_CSS_END_TAG)); } }
					 * catch (NullPointerException e) {
					 * 
					 * sr.setNameToView(doc.getFieldValue("identifier").toString
					 * ().toLowerCase().replaceAll( "\\b" + q.toLowerCase() + "\\b",
					 * HIGHLIGHTER_CSS_START_TAG + q.toLowerCase() + HIGHLIGHTER_CSS_END_TAG));
					 * 
					 * }
					 */
					searchResult.add(sr);

				}

				// Facet Fields
				/*
				 * List<FacetField> facetFields = queryResponse.getFacetFields();
				 * 
				 * HashMap<String, List<FacetNameCountHolder>> facetMap = new HashMap<>();
				 * List<FacetNameCountHolder> langfacetList = new
				 * ArrayList<FacetNameCountHolder>(); List<FacetNameCountHolder>
				 * hostnameFacetList = new ArrayList<FacetNameCountHolder>(); for (FacetField
				 * facetField : facetFields) { if
				 * (facetField.getName().equalsIgnoreCase("lang")) {
				 * 
				 * for (Count count : facetField.getValues()) // for lang { facetNameCountHolder
				 * = new FacetNameCountHolder(); facetNameCountHolder.setName(count.getName());
				 * facetNameCountHolder.setCount((int) count.getCount());
				 * langfacetList.add(facetNameCountHolder); }
				 * 
				 * } else if (facetField.getName().equalsIgnoreCase("baseUrl")) { // for //
				 * hostname for (Count count : facetField.getValues()) { facetNameCountHolder =
				 * new FacetNameCountHolder(); facetNameCountHolder.setName(count.getName());
				 * facetNameCountHolder.setCount((int) count.getCount());
				 * hostnameFacetList.add(facetNameCountHolder); }
				 * 
				 * } }
				 */

				/*
				 * facetMap.put("facet_languages", langfacetList);
				 * facetMap.put("facet_institution", hostnameFacetList);
				 */
				logger.log(Level.INFO, "Result: " + searchResult.toString());
				searchForm.setResultSize(queryResponse.getResults().getNumFound());
				searchForm.setResultFound(true);
				searchForm.setListOfResult(searchResult);
				// searchForm.setFacetMap(facetMap);

				return searchForm;
			} else {
				throw new Exception();
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		searchForm.setResultSize((long) 0);
		searchForm.setResultFound(false);
		searchForm.setListOfResult(searchResult);
		return searchForm;

	}

	/**
	 * @param searchFrm_recieved
	 * @param pageNum
	 * @param pageWin
	 * @return
	 */
	@POST
	@Path("/basicSearch/{pageNum}/{pageWin}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public SearchForm getListOfResultFromSearchForm(SearchForm searchFrm_recieved, @PathParam("pageNum") String pageNum,
			@PathParam("pageWin") String pageWin, @QueryParam("language") String requestLanguage) {
		logger.log(Level.INFO, "path encountered: basicSearch/pageNum/pageWin");
		logger.log(Level.INFO, "--In getListOfResultFromSearchForm at basicSearch--");
		// Init Searchform for HCDC
		logger.log(Level.INFO, "recieved resource code:" + searchFrm_recieved.getResourceCode());
		logger.log(Level.INFO, "Recieved Langauge from Request: " + requestLanguage);
		if (searchFrm_recieved.getSearchComplexity().equals(SearchComplexityEnum.ADVANCED)) {
			/* For future, advanced search complexity */
			return initSearchForm(searchFrm_recieved);
		}
		SearchForm searchForm = initSearchForm(searchFrm_recieved);
		logger.log(Level.INFO, "init resource code in new searchform:" + searchForm.getResourceCode());

		String query = searchForm.getSearchElement();
		query = processingDao.cleanQuery(query);

		Map<String, List<String>> filterMap = new HashMap<>();
		Map<String, List<String>> specificFilterMap = new HashMap<>();

		if (searchForm.getFilters() != null)

		{
			filterMap = searchForm.getFilters();

		}
		if (searchForm.getSpecificFilters() != null)
			specificFilterMap = searchForm.getSpecificFilters();
		searchResult = new ArrayList<SearchResult>();

		logger.log(Level.INFO, "Got input: q:" + query);

		String lang = "";

		try {
			if (!processingDao.queryIntialization(query, filterMap, pageNum, pageWin, searchForm.getResourceCode(),
					facetfields, requestLanguage))
				throw new Exception();

			if (filterMap != null) {

				processingDao.extractFilterMapAndAddInQuery(filterMap);

			}
			if (specificFilterMap != null) {

				processingDao.extractFilterMapAndAddInQuery(specificFilterMap);

			}
			if (searchForm.getSubResourceIdentifiers() != null && !searchForm.getSubResourceIdentifiers().isEmpty()) {

				processingDao.extractSubResourceIdentifierListAndAddInQuery(searchForm.getSubResourceIdentifiers(),
						searchForm.getResourceCode());

			}
			if (!searchForm.getResourceCode().equals("")) {

				processingDao.getSolrServer(searchForm.getResourceCode());
			}

			queryResponse = processingDao.queryResponse();
			SolrDocumentList solrDocumentList = queryResponse.getResults();
			Map<String, Map<String, List<String>>> highlightingMap = queryResponse.getHighlighting() != null
					? queryResponse.getHighlighting()
					: new HashMap<>();
			if (solrDocumentList != null) {

				for (SolrDocument doc : solrDocumentList) {
					if (doc != null) {

						SearchResult sr = new SearchResult();
						// Create Objects if required
						List<String> title = new ArrayList<String>();
						List<String> language = new ArrayList<String>();
						List<String> description = new ArrayList<String>();
						List<String> source = new ArrayList<String>();
						List<String> date = new ArrayList<String>();
						try {
							// Populate the arraylists of objects if required
							// per doc

							language.add(doc.getFieldValue("lang") != null ? doc.getFieldValue("lang").toString() : "");
							source.add(doc.getFieldValue("url") != null ? doc.getFieldValue("url").toString() : "");

							Map<String, List<String>> highlightedContent = highlightingMap
									.get(doc.getFieldValue("id").toString()) != null
											? highlightingMap.get(doc.getFieldValue("id").toString())
											: new HashMap<>();
							String content = "";
							String titleString = "";
							if (highlightedContent != null && !highlightedContent.isEmpty()) {
								if (highlightedContent.get("content") != null
										&& !highlightedContent.get("content").isEmpty())
									for (String snippet : highlightedContent.get("content")) {
										content += snippet + "...";
									}
								else {
									if (!query.equalsIgnoreCase("*.*")) {
										content = doc.getFieldValue("content") != null ? doc.getFieldValue("content")
												.toString().replaceAll("\n", " ").toLowerCase().replaceAll(
														"\\b" + query.toLowerCase() + "\\b", HIGHLIGHTER_CSS_START_TAG
																+ query.toLowerCase() + HIGHLIGHTER_CSS_END_TAG)
												: "";
									} else {
										content = doc.getFieldValue("content") != null
												? doc.getFieldValue("content").toString().replaceAll("\n", " ")
												: "";
									}
								}
								if (highlightedContent.get("title") != null
										&& !highlightedContent.get("title").isEmpty())
									titleString = highlightedContent.get("title").get(0);
								else
									titleString = doc.getFieldValue("title")!=null?doc.getFieldValue("title").toString():"";
							} else {
								if (!query.equalsIgnoreCase("*.*")) {
									content = doc.getFieldValue("content") != null ? doc.getFieldValue("content")
											.toString().replaceAll("\n", " ").toLowerCase()
											.replaceAll("\\b" + query.toLowerCase() + "\\b", HIGHLIGHTER_CSS_START_TAG
													+ query.toLowerCase() + HIGHLIGHTER_CSS_END_TAG)
											: "";
								} else {
									content = doc.getFieldValue("content") != null
											? doc.getFieldValue("content").toString().replaceAll("\n", " ")
											: "";
								}
								titleString = doc.getFieldValue("title") != null ? doc.getFieldValue("title").toString()
										: "";
							}

							title.add(titleString);
							description.add(content);
							String articleDate = doc.getFieldValue("articleDate") != null
									? doc.getFieldValue("articleDate").toString()
									: "";
							if (!articleDate.equals(""))
								date.add(processingDao.getDate(articleDate));
							else
								date.add("");

							// Set the Objects in Search Result, that are made
							// above, or
							// else set the direct results from JSON object from
							// Solr

							if (searchForm.getResourceCode().equalsIgnoreCase("ENEW"))
								sr.setData(date);
							else if (searchForm.getResourceCode().equalsIgnoreCase("GOVW"))
								sr.setData(null);

							sr.setResourceType(doc.getFieldValue("resourceType") != null
									? doc.getFieldValue("resourceType").toString()
									: "");
							sr.setRecordIdentifier(
									doc.getFieldValue("identifier") != null ? doc.getFieldValue("identifier").toString()
											: "");
							sr.setTitle(title);
							sr.setLanguage(language);
							sr.setDescription(description);
							sr.setSource(source);

							try {
								if (!titleString.equals("")) {
									sr.setNameToView(titleString);
								} else {
									sr.setNameToView(doc.getFieldValue("identifier") != null
											? doc.getFieldValue("identifier").toString().toLowerCase().replaceAll(
													"\\b" + query.toLowerCase() + "\\b", HIGHLIGHTER_CSS_START_TAG
															+ query.toLowerCase() + HIGHLIGHTER_CSS_END_TAG)
											: "");
								}
							} catch (NullPointerException e) {

								sr.setNameToView(doc.getFieldValue("identifier") != null
										? doc.getFieldValue("identifier").toString().toLowerCase().replaceAll(
												"\\b" + query.toLowerCase() + "\\b", HIGHLIGHTER_CSS_START_TAG
														+ query.toLowerCase() + HIGHLIGHTER_CSS_END_TAG)
										: "");

							}
							searchResult.add(sr);
						} catch (NullPointerException e) {
							e.printStackTrace();
							continue;
						}
					}
				}

				// Facet Fields
				List<FacetField> facetFields = queryResponse.getFacetFields();

				HashMap<String, List<FacetNameCountHolder>> facetMap = new HashMap<>();
				List<FacetNameCountHolder> langFacetList = new ArrayList<FacetNameCountHolder>();
				List<FacetNameCountHolder> hostnameFacetList = new ArrayList<FacetNameCountHolder>();
				List<FacetNameCountHolder> wikicodeFacetList = new ArrayList<FacetNameCountHolder>();
				List<FacetNameCountHolder> domainFacetList = new ArrayList<FacetNameCountHolder>();
				for (FacetField facetField : facetFields) {
					if (facetField.getName().equalsIgnoreCase("lang")) {

						for (Count count : facetField.getValues()) // for lang
						{
							facetNameCountHolder = new FacetNameCountHolder();
							facetNameCountHolder.setName(count.getName());
							facetNameCountHolder.setCount((int) count.getCount());
							if (facetNameCountHolder.getCount() != 0)
								langFacetList.add(facetNameCountHolder);
						}

					} else if (facetField.getName().equalsIgnoreCase("hostname")) { // for
																					// hostname
						for (Count count : facetField.getValues()) {
							facetNameCountHolder = new FacetNameCountHolder();
							facetNameCountHolder.setName(count.getName());
							facetNameCountHolder.setCount((int) count.getCount());
							if (facetNameCountHolder.getCount() != 0)
								hostnameFacetList.add(facetNameCountHolder);
						}

					} else if (facetField.getName().equalsIgnoreCase("idcode")
							&& searchForm.getResourceCode().equals("WIKI")) {

						for (Count count : facetField.getValues()) // for lang
						{
							facetNameCountHolder = new FacetNameCountHolder();
							facetNameCountHolder.setName(count.getName().toUpperCase());
							facetNameCountHolder.setCount((int) count.getCount());
							if (facetNameCountHolder.getCount() != 0)
								wikicodeFacetList.add(facetNameCountHolder);
						}

					} else if (facetField.getName().equalsIgnoreCase("domain")
							&& searchForm.getResourceCode().equals("ENEW")) { // check
																				// for
																				// GOVW
																				// too

						for (Count count : facetField.getValues()) // for lang
						{
							facetNameCountHolder = new FacetNameCountHolder();
							facetNameCountHolder.setName(count.getName());
							facetNameCountHolder.setCount((int) count.getCount());
							if (facetNameCountHolder.getCount() != 0)
								domainFacetList.add(facetNameCountHolder);
						}

					}
				}

				facetMap.put("facet_languages", langFacetList);
				facetMap.put("facet_hostname", hostnameFacetList);
				facetMap.put("facet_wikicode", wikicodeFacetList);
				facetMap.put("facet_domain", domainFacetList);
				logger.log(Level.INFO, "Result: " + searchResult.toString());
				searchForm.setResultSize(queryResponse.getResults().getNumFound());
				searchForm.setResultFound(true);
				searchForm.setListOfResult(searchResult);
				searchForm.setFacetMap(facetMap);

				return searchForm;
			} else {
				throw new Exception();
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		searchForm.setResultSize((long) 0);
		searchForm.setResultFound(false);
		searchForm.setListOfResult(searchResult);
		// searchForm.setResourceCode(searchFrm_recieved.getResourceCode());
		return searchForm;

	}

	/**
	 * @param searchFrm_recieved
	 * @param pageNum
	 * @param pageWin
	 * @return
	 */
	@POST
	@Path("/vasSearch/{pageNum}/{pageWin}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public SearchForm getListOfVasResultFromSearchForm(SearchForm searchFrm_recieved,
			@PathParam("pageNum") String pageNum, @PathParam("pageWin") String pageWin,
			@QueryParam("language") String requestLanguage) {
		logger.log(Level.INFO, "path encountered: vasSearch/pageNum/pageWin");
		logger.log(Level.INFO, "--In getListOfVasResultFromSearchForm at vasSearch--");
		// Init Searchform for HCDC
		logger.log(Level.INFO, "recieved resource code:" + searchFrm_recieved.getResourceCode());
		logger.log(Level.INFO, "Recieved Langauge from Request: " + requestLanguage);
		if (searchFrm_recieved.getSearchComplexity().equals(SearchComplexityEnum.ADVANCED)) {
			/* For future, advanced search complexity */
			return initSearchForm(searchFrm_recieved);
		}
		SearchForm searchForm = initSearchForm(searchFrm_recieved);
		logger.log(Level.INFO, "init resource code in new searchform:" + searchForm.getResourceCode());

		String query = searchForm.getSearchElement();
		query = processingDao.cleanQuery(query);

		Map<String, List<String>> filterMap = new HashMap<>();
		Map<String, List<String>> specificFilterMap = new HashMap<>();

		if (searchForm.getFilters() != null)
			filterMap = searchForm.getFilters();
		if (searchForm.getSpecificFilters() != null)
			specificFilterMap = searchForm.getSpecificFilters();
		searchResult = new ArrayList<SearchResult>();

		logger.log(Level.INFO, "Got input: q:" + query);

		String lang = "";

		try {
			if (!processingDao.queryIntialization(query, filterMap, pageNum, pageWin, searchForm.getResourceCode(),
					facetfields, requestLanguage))
				throw new Exception();

			if (filterMap != null) {

				processingDao.extractFilterMapAndAddInQuery(filterMap);

			}
			if (specificFilterMap != null) {

				processingDao.extractFilterMapAndAddInQuery(specificFilterMap);

			}
			if (searchForm.getSubResourceIdentifiers() != null && !searchForm.getSubResourceIdentifiers().isEmpty()) {

				processingDao.extractSubResourceIdentifierListAndAddInQuery(searchForm.getSubResourceIdentifiers(),
						searchForm.getResourceCode());

			}
			if (!searchForm.getResourceCode().equals("")) {

				processingDao.getSolrServer(searchForm.getResourceCode());
			}

			queryResponse = processingDao.queryResponse();
			SolrDocumentList solrDocumentList = queryResponse.getResults();

			System.out.println("solr document size is" + solrDocumentList.getNumFound());

			/* 29 june 2017 for highlighting */
			Map<String, Map<String, List<String>>> highlightingMap = queryResponse.getHighlighting() != null
					? queryResponse.getHighlighting()
					: new HashMap<>();

			if (solrDocumentList != null) {

				for (SolrDocument doc : solrDocumentList) {

					if (doc != null) {
						SearchResult sr = new SearchResult();
						// Create Objects if required
						List<String> title = new ArrayList<String>();
						List<String> description = new ArrayList<String>();
						List<String> source = new ArrayList<String>();
						List<String> con_date = new ArrayList<String>();
						List<String> awd_recipentName = new ArrayList<String>();
						List<String> awd_Year = new ArrayList<String>();
						List<String> awd_Field = new ArrayList<String>();
						// Populate the arraylists of objects if required per
						// doc
						try {

							Map<String, List<String>> highlightedContent = highlightingMap
									.get(doc.getFieldValue("id").toString()) != null
											? highlightingMap.get(doc.getFieldValue("id").toString())
											: new HashMap<>();
							String content = "";
							String titleString = "";
							String awdRcpName = "";
							String awdYear = "";
							String awdField = "";
							String location = "";

							if (highlightedContent != null && !highlightedContent.isEmpty()) {
								if (highlightedContent.get("content") != null
										&& !highlightedContent.get("content").isEmpty()) {
									for (String snippet : highlightedContent.get("content")) {
										content += snippet + "...";
									}
								} else {
									if (!query.equalsIgnoreCase("*.*")) {
										content = doc.getFieldValue("content")!=null?doc.getFieldValue("content").toString().replaceAll("\n", " ")
												.toLowerCase().replaceAll("\\b" + query.toLowerCase() + "\\b",
														HIGHLIGHTER_CSS_START_TAG + query.toLowerCase()
																+ HIGHLIGHTER_CSS_END_TAG):"";
									} else {
										content = doc.getFieldValue("content")!=null?doc.getFieldValue("content").toString().replaceAll("\n", " "):"";
									}
								}
								if (highlightedContent.get("title") != null
										&& !highlightedContent.get("title").isEmpty())
									titleString = highlightedContent.get("title").get(0);
								else
									titleString = doc.getFieldValue("title").toString();

								if (doc.getFieldValue("idcode").toString().equalsIgnoreCase("VA_AWD")) {

									if (highlightedContent.get("awardRecipientName") != null
											&& !highlightedContent.get("awardRecipientName").isEmpty())
										awdRcpName = highlightedContent.get("awardRecipientName").get(0);
									else
										awdRcpName = doc.getFieldValue("awardRecipientName").toString();

									if (highlightedContent.get("awardField") != null
											&& !highlightedContent.get("awardField").isEmpty())
										awdField = highlightedContent.get("awardField").get(0);
									else
										awdField = doc.getFieldValue("awardField").toString();

									if (highlightedContent.get("location") != null
											&& !highlightedContent.get("location").isEmpty())
										location = highlightedContent.get("location").get(0);
									else
										location = doc.getFieldValue("location").toString();

									awd_Year.add(processingDao.getDate(doc.getFieldValue("date").toString()));

									awd_recipentName.add(awdRcpName);
									awd_Field.add(awdField);

									sr.setCreator(awd_recipentName);
									sr.setData(awd_Year);
									sr.setType(awd_Field);
								}
								if (doc.getFieldValue("idcode").toString().equalsIgnoreCase("VA_CON")) {
									if (highlightedContent.get("location") != null
											&& !highlightedContent.get("location").isEmpty())
										location = highlightedContent.get("location").get(0);
									else
										location = doc.getFieldValue("location").toString();

									content = titleString; // bcoz conferences dont have description
									con_date.add(processingDao.getDate(doc.getFieldValue("date").toString()));
									sr.setData(con_date);
								}
								if (doc.getFieldValue("idcode").toString().equalsIgnoreCase("VA_SEM")) {
									if (highlightedContent.get("location") != null
											&& !highlightedContent.get("location").isEmpty())
										location = highlightedContent.get("location").get(0);
									else
										location = doc.getFieldValue("location").toString();

									content = titleString; // bcoz conferences dont have description
									con_date.add(processingDao.getDate(doc.getFieldValue("date").toString()));
									sr.setData(con_date);
								}
							} else {
								if (!query.equalsIgnoreCase("*.*")) {
									content = doc.getFieldValue("content").toString().replaceAll("\n", " ")
											.toLowerCase()
											.replaceAll("\\b" + query.toLowerCase() + "\\b", HIGHLIGHTER_CSS_START_TAG
													+ query.toLowerCase() + HIGHLIGHTER_CSS_END_TAG);
								} else {
									content = doc.getFieldValue("content").toString().replaceAll("\n", " ");
								}
								titleString = doc.getFieldValue("title").toString();

								if (doc.getFieldValue("idcode").toString().equalsIgnoreCase("VA_AWD")) {

									awdRcpName = doc.getFieldValue("awardRecipientName").toString();
									awdField = doc.getFieldValue("awardField").toString();
									location = doc.getFieldValue("location").toString();
									awd_Year.add(processingDao.getDate(doc.getFieldValue("date").toString()));

									awd_recipentName.add(awdRcpName);
									awd_Field.add(awdField);

									sr.setCreator(awd_recipentName);
									sr.setData(awd_Year);
									sr.setType(awd_Field);
								}

								if (doc.getFieldValue("idcode").toString().equalsIgnoreCase("VA_CON")) {
									location = doc.getFieldValue("location").toString();
									content = titleString; // bcoz conferences dont have description
									con_date.add(processingDao.getDate(doc.getFieldValue("date").toString()));
									sr.setData(con_date);
								}
								if (doc.getFieldValue("idcode").toString().equalsIgnoreCase("VA_SEM")) {
									location = doc.getFieldValue("location").toString();
									content = titleString; // bcoz conferences dont have description
									con_date.add(processingDao.getDate(doc.getFieldValue("date").toString()));
									sr.setData(con_date);
								}
							}

							source.add(doc.getFieldValue("url").toString());
							source.add(location);
							title.add(titleString);
							description.add(content);

							sr.setResourceType(doc.getFieldValue("resourceType").toString());
							sr.setRecordIdentifier(doc.getFieldValue("identifier").toString());

							sr.setTitle(title);
							sr.setDescription(description);
							sr.setSource(source);

							try {
								if (!doc.getFieldValue("title").toString().equals(null)
										&& !doc.getFieldValue("title").toString().equals("")) {

									sr.setNameToView(doc.getFieldValue("title").toString());
								} else {
									sr.setNameToView(doc.getFieldValue("identifier").toString().toLowerCase()
											.replaceAll("\\b" + query.toLowerCase() + "\\b", HIGHLIGHTER_CSS_START_TAG
													+ query.toLowerCase() + HIGHLIGHTER_CSS_END_TAG));
								}
							} catch (NullPointerException e) {

								sr.setNameToView(doc.getFieldValue("identifier").toString().toLowerCase().replaceAll(
										"\\b" + query.toLowerCase() + "\\b",
										HIGHLIGHTER_CSS_START_TAG + query.toLowerCase() + HIGHLIGHTER_CSS_END_TAG));

							}

							searchResult.add(sr);
						} catch (NullPointerException e) {
							logger.log(Level.SEVERE, e.getMessage());
							continue;
						}
					}
				}
				// Facet Fields
				List<FacetField> facetFields = queryResponse.getFacetFields();

				HashMap<String, List<FacetNameCountHolder>> facetMap = new HashMap<>();
				List<FacetNameCountHolder> vascodefacetList = new ArrayList<FacetNameCountHolder>();

				for (FacetField facetField : facetFields) {
					if (facetField.getName().equalsIgnoreCase("idcode")
							&& searchForm.getResourceCode().equals("VALUE")) {

						for (Count count : facetField.getValues()) {
							facetNameCountHolder = new FacetNameCountHolder();
							System.out.println(
									"vascode to ui is***" + processingDao.findVasValue(count.getName().toUpperCase()));
							facetNameCountHolder.setName(processingDao.findVasValue(count.getName().toUpperCase()));
							facetNameCountHolder.setCount((int) count.getCount());
							if (facetNameCountHolder.getCount() != 0)
								vascodefacetList.add(facetNameCountHolder);
						}

					}
				}

				facetMap.put("facet_vascode", vascodefacetList);
				logger.log(Level.INFO, "Result: " + searchResult.toString());
				searchForm.setResultSize(queryResponse.getResults().getNumFound());
				searchForm.setResultFound(true);
				searchForm.setListOfResult(searchResult);
				searchForm.setFacetMap(facetMap);

				return searchForm;
			} else {
				throw new Exception();
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		searchForm.setResultSize((long) 0);
		searchForm.setResultFound(false);
		searchForm.setListOfResult(searchResult);

		return searchForm;

	}

	/**
	 * @return
	 */
	@POST
	@Path("/ENEW")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, Long> getNewsCount() {
		logger.log(Level.INFO, "path encountered: ENEW");

		HashMap<String, Long> newsCountmap = new HashMap<>();
		String q = "*.*";
		String lang = "";
		logger.log(Level.INFO, "Got input: q:" + q);
		try {
			if (!processingDao.queryIntialization(q, "ENEW"))
				throw new Exception();
			// Get Solr Server from Resource Type

			processingDao.getSolrServer("ENEW");
			queryResponse = processingDao.queryResponse();
			newsCountmap.put("ENEW", queryResponse.getResults().getNumFound());

		} catch (Exception e) {
			e.printStackTrace();

		}
		return newsCountmap;

	}

	/**
	 * @return
	 */
	@POST
	@Path("/GOVW")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, Long> getGovCount() {
		logger.log(Level.INFO, "path encountered: GOVW");

		HashMap<String, Long> govwCountmap = new HashMap<>();
		String q = "*.*";
		String lang = "";
		logger.log(Level.INFO, "Got input: q:" + q);
		try {
			if (!processingDao.queryIntialization(q, "GOVW"))
				throw new Exception();
			// Get Solr Server from Resource Type

			processingDao.getSolrServer("GOVW");
			queryResponse = processingDao.queryResponse();
			govwCountmap.put("GOVW", queryResponse.getResults().getNumFound());

		} catch (Exception e) {
			e.printStackTrace();

		}
		return govwCountmap;

	}

	// get total wiki count
	/**
	 * @return
	 */
	@POST
	@Path("/WIKI")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, Long> getWikiCount() {

		HashMap<String, Long> wikiCountmap = new HashMap<>();

		// String[] lsStr = { "en", "as", "bn", "gu", "pu", "mr", "or", "ta",
		// "te", "hi" };
		long wikiFullCount = 0;
		String q = "*.*";
		logger.log(Level.INFO, "Got input: q:" + q);
		/*
		 * try { if (!processingDao.queryIntialization(q)) throw new Exception(); // Get
		 * Solr Server from Resource Type for (String lang : lsStr) {
		 */
		try {
			processingDao.queryIntialization(q, "WIKI");
			processingDao.getSolrServer("WIKI");
		} catch (IOException e) {

			e.printStackTrace();
		}
		queryResponse = processingDao.queryResponse();
		wikiFullCount = queryResponse.getResults().getNumFound();

		/*
		 * }
		 */
		wikiCountmap.put("WIKI", wikiFullCount);

		return wikiCountmap;
		/*
		 * } catch (Exception e) { e.printStackTrace();
		 * 
		 * } return wikiCountmap;
		 */
	}

	// get count of wiki per lang
	/**
	 * @param langcode
	 * @return
	 */
	@POST
	@Path("/WIKICOUNT/{langcode}")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, Long> getWikiCountPerLang(@PathParam("langcode") String langcode) {

		HashMap<String, Long> wikiCountmap = new HashMap<>();
		String q = "*.*";
		logger.log(Level.INFO, "Got input: q:" + q);
		// Extract lang from langcode

		String lang = langcode.substring(4).toLowerCase();

		logger.log(Level.INFO, "Got lang for WIKI: lang:" + lang);
		try {
			if (!processingDao.queryIntialization(q, "WIKI"))
				throw new Exception();
			// Get Solr Server from Resource Type
			processingDao.getSolrServer("WIKI");

			processingDao.addFilterQuery("lang", lang);
			queryResponse = processingDao.queryResponse();

			wikiCountmap.put(langcode, queryResponse.getResults().getNumFound());

			return wikiCountmap;

		} catch (Exception e) {
			e.printStackTrace();

		}
		return wikiCountmap;

	}

	// get total vas count
	/**
	 * @return
	 */
	@POST
	@Path("/VALUE")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, Long> getVasCount() {

		HashMap<String, Long> vasCountmap = new HashMap<>();

		long vasFullCount = 0;
		String q = "*.*";
		logger.log(Level.INFO, "Got input: q:" + q);

		try {
			processingDao.queryIntialization(q, "VALUE");

			processingDao.getSolrServer("VALUE");
			queryResponse = processingDao.queryResponse();
			vasFullCount = queryResponse.getResults().getNumFound();
		} catch (IOException e) {

			e.printStackTrace();
		}

		vasCountmap.put("VALUE", vasFullCount);

		return vasCountmap;

	}

	// get vas count per value added service
	/**
	 * @param vasCode
	 * @return
	 */
	@POST
	@Path("/VALUECOUNT/{vasCode}")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, Long> getVasCountPerVas(@PathParam("vasCode") String vasCode) {

		HashMap<String, Long> vasCountmap = new HashMap<>();

		long vasCount = 0;
		String q = "*.*";
		logger.log(Level.INFO, "Got input: q:" + q);

		try {
			processingDao.queryIntialization(q, "VALUE");
			processingDao.getSolrServer("VALUE");
			processingDao.addFilterQuery("idcode", vasCode);

			queryResponse = processingDao.queryResponse();
			vasCount = queryResponse.getResults().getNumFound();
		} catch (IOException e) {

			e.printStackTrace();
		}

		vasCountmap.put(vasCode, vasCount);

		return vasCountmap;

	}

	/**
	 * @return
	 */
	@POST
	@Path("/ENEWHOST")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, String> getNewsHostFacetCount() {
		logger.log(Level.INFO, "path encountered: ENEWHOST");

		HashMap<String, String> enewsHostCountmap = new HashMap<>();
		String q = "*.*";
		String lang = "";
		logger.log(Level.INFO, "Got input: q:" + q);
		try {
			if (!processingDao.queryIntialization(q, "ENEW"))
				throw new Exception();
			// Get Solr Server from Resource Type
			processingDao.addFacetField("hostname");

			processingDao.getSolrServer("ENEW");
			queryResponse = processingDao.queryResponse();

			// Facet Fields
			List<FacetField> facetFields = queryResponse.getFacetFields();

			for (FacetField facetField : facetFields) {
				if (facetField.getName().equalsIgnoreCase("hostname")) { // for
																			// hostname
					for (Count count : facetField.getValues()) {
						if (count.getCount() != 0) {
							String name = count.getName();
							String Count = String.valueOf(count.getCount());
							enewsHostCountmap.put(name, Count);
						}

					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return enewsHostCountmap;

	}

	/**
	 * @return
	 */
	@POST
	@Path("/ENEWIDCODE")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, String> getNewsIdcodeFacetCount() {
		logger.log(Level.INFO, "path encountered: ENEWIDCODE");

		HashMap<String, String> enewsIdcodeCountmap = new HashMap<>();
		String q = "*.*";
		String lang = "";
		logger.log(Level.INFO, "Got input: q:" + q);
		try {
			if (!processingDao.queryIntialization(q, "ENEW"))
				throw new Exception();
			// Get Solr Server from Resource Type
			processingDao.addFacetField("idcode");

			processingDao.getSolrServer("ENEW");
			queryResponse = processingDao.queryResponse();

			// Facet Fields
			List<FacetField> facetFields = queryResponse.getFacetFields();

			for (FacetField facetField : facetFields) {
				if (facetField.getName().equalsIgnoreCase("idcode")) { // for
																		// idcode
					for (Count count : facetField.getValues()) {
						// changed on 27 oct
						/*
						 * if (count.getCount() == 0) { String name = count.getName(); String Count =
						 * null; enewsIdcodeCountmap.put(name, Count); } else{ String name =
						 * count.getName(); String Count = String.valueOf(count.getCount());
						 * enewsIdcodeCountmap.put(name, Count); }
						 */
						if (count.getCount() != 0) {
							String name = count.getName();
							String Count = String.valueOf(count.getCount());

							enewsIdcodeCountmap.put(name, Count);
						}

					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return enewsIdcodeCountmap;

	}

	/**
	 * @return
	 */
	@POST
	@Path("/GOVWHOST")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, String> getGovwHostFacetCount() {
		logger.log(Level.INFO, "path encountered: GOVWHOST");

		HashMap<String, String> govwHostCountmap = new HashMap<>();
		String q = "*.*";
		String lang = "";
		logger.log(Level.INFO, "Got input: q:" + q);
		try {
			if (!processingDao.queryIntialization(q, "GOVW"))
				throw new Exception();
			// Get Solr Server from Resource Type
			processingDao.addFacetField("hostname");

			processingDao.getSolrServer("GOVW");
			queryResponse = processingDao.queryResponse();

			// Facet Fields
			List<FacetField> facetFields = queryResponse.getFacetFields();

			for (FacetField facetField : facetFields) {
				if (facetField.getName().equalsIgnoreCase("hostname")) { // for
																			// hostname
					for (Count count : facetField.getValues()) {
						if (count.getCount() != 0) {
							String name = count.getName();
							String Count = String.valueOf(count.getCount());
							govwHostCountmap.put(name, Count);
						}

					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return govwHostCountmap;

	}

	/**
	 * @return
	 */
	@POST
	@Path("/GOVWIDCODE")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, String> getGovwIdcodeFacetCount() {
		logger.log(Level.INFO, "path encountered: GOVWIDCODE");

		HashMap<String, String> govwIdcodeCountmap = new HashMap<>();
		String q = "*.*";
		String lang = "";
		logger.log(Level.INFO, "Got input: q:" + q);
		try {
			if (!processingDao.queryIntialization(q, "GOVW"))
				throw new Exception();
			// Get Solr Server from Resource Type
			processingDao.addFacetField("idcode");

			processingDao.getSolrServer("GOVW");
			queryResponse = processingDao.queryResponse();

			// Facet Fields
			List<FacetField> facetFields = queryResponse.getFacetFields();

			for (FacetField facetField : facetFields) {
				if (facetField.getName().equalsIgnoreCase("idcode")) { // for
																		// idcode
					for (Count count : facetField.getValues()) {
						// changed on 27 oct
						if (count.getCount() == 0) {
							String name = count.getName();
							String Count = null;
							govwIdcodeCountmap.put(name, Count);
						} else {
							String name = count.getName();
							String Count = String.valueOf(count.getCount());
							govwIdcodeCountmap.put(name, Count);
						}
						/*
						 * if (count.getCount() != 0) { String name = count.getName(); String Count =
						 * String.valueOf(count.getCount()); govwIdcodeCountmap.put(name, Count); }
						 */

					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return govwIdcodeCountmap;

	}

	/**
	 * @param code
	 * @return
	 */
	@POST
	@Path("/ENEWIDCODE/{code}")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, String> getNewsIdcodeFacetCount(@PathParam("code") String code) {
		logger.log(Level.INFO, "path encountered: ENEWIDCODE/CODE");

		HashMap<String, String> enewsIdcodeCountmap = new HashMap<>();
		String q = "*.*";
		String lang = "";
		logger.log(Level.INFO, "Got input: q:" + q);
		try {
			if (!processingDao.queryIntialization(q, "ENEW"))
				throw new Exception();
			// Get Solr Server from Resource Type
			processingDao.addFacetField("idcode");

			if (!processingDao.addFilterQuery("idcode", code)) // adding code in
				// filter query
				throw new Exception();
			processingDao.getSolrServer("ENEW");
			queryResponse = processingDao.queryResponse();
			if (queryResponse != null) {
				enewsIdcodeCountmap.put(code, String.valueOf(queryResponse.getResults().getNumFound()));
			} else {
				enewsIdcodeCountmap.put("", "");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return enewsIdcodeCountmap;

	}

	/**
	 * @param code
	 * @return
	 */
	@POST
	@Path("/GOVWIDCODE/{code}")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, String> getGovwIdcodeFacetCount(@PathParam("code") String code) {
		logger.log(Level.INFO, "path encountered: GOVWIDCODE/CODE");

		HashMap<String, String> govwIdcodeCountmap = new HashMap<>();
		String q = "*.*";
		String lang = "";
		logger.log(Level.INFO, "Got input: q:" + q);
		try {
			if (!processingDao.queryIntialization(q, "GOVW"))
				throw new Exception();
			// Get Solr Server from Resource Type
			processingDao.addFacetField("idcode");

			if (!processingDao.addFilterQuery("idcode", code)) // adding code in
				// filter query
				throw new Exception();
			processingDao.getSolrServer("GOVW");
			queryResponse = processingDao.queryResponse();
			if (queryResponse != null) {
				govwIdcodeCountmap.put(code, String.valueOf(queryResponse.getResults().getNumFound()));
			} else {
				govwIdcodeCountmap.put("", "");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return govwIdcodeCountmap;

	}

	/**
	 * @param resourceType
	 * @param Term
	 * @return
	 */

	public List<String> getDidYouMeanOld(@PathParam("resourceType") String resourceType,
			@PathParam("Term") String Term) {
		logger.log(Level.INFO, "path encountered: SIMILAR/RESOURCETYPE/TERM");
		// System.out.println("Term: "+Term);
		StringTokenizer stringTokenizer = new StringTokenizer(Term, " ");
		for (int i = 0; i < resourceTypeCodes.length; i++) {
			if (resourceType.equalsIgnoreCase(resourceTypeCodes[i])) {
				logger.log(Level.INFO, "SIMILAR TERMS resourceType: " + resourceType);
				break;
			}
		}

		// number of suggestions
		final int suggestionNumber = 5;
		// get the suggested words
		String[] final_suggestions = new String[] { "" };
		while (stringTokenizer.hasMoreTokens()) {
			String[] suggestions = null;
			String tempterm = stringTokenizer.nextToken();
			try {

				suggestions = ProcessingDao.sp.suggestSimilar(tempterm, suggestionNumber);
			} catch (IOException e) {
				e.printStackTrace();
			}

			final_suggestions = ProcessingDao.cartesianProduct(final_suggestions, suggestions);

		}
		for (int i = 0; i < final_suggestions.length; i++) {
			final_suggestions[i] = final_suggestions[i].trim();
		}
		ArrayList<String> final_returned_list_from_solr = new ArrayList<>();
		String q = "";

		for (int i = 0; i < final_suggestions.length; i++) {
			q = final_suggestions[i];
			System.out.println("suggessted term is" + q);
			try {
				if (!processingDao.queryIntialization(q, resourceType))
					throw new Exception();
				// Get Solr Server from Resource Type

				processingDao.getSolrServer(resourceType);
				queryResponse = processingDao.queryResponse();
				if (queryResponse.getResults().getNumFound() > 0) {
					final_returned_list_from_solr.add(q);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return final_returned_list_from_solr;

	}

	/**
	 * @param resourceType
	 * @param Term
	 * @return
	 */
	@GET
	@Path("/similar/{resourceType}/{Term}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<String> getDidYouMean(@PathParam("resourceType") String resourceType, @PathParam("Term") String Term) {
		logger.log(Level.INFO, "path encountered: SIMILAR/RESOURCETYPE/TERM");
		Term = processingDao.cleanQuery(Term);
		StringTokenizer stringTokenizer = new StringTokenizer(Term, " ");
		String finalString = "";
		List<String> returnList = new ArrayList<>();
		for (int i = 0; i < resourceTypeCodes.length; i++) {
			if (resourceType.equalsIgnoreCase(resourceTypeCodes[i])) {
				logger.log(Level.INFO, "SIMILAR TERMS resourceType: " + resourceType);
				break;
			}
		}
		String[] suggestions = new String[stringTokenizer.countTokens()];
		int j = 0;
		while (stringTokenizer.hasMoreTokens()) {

			String tempterm = stringTokenizer.nextToken();
			suggestions[j++] = ProcessingDao.sc.correct(tempterm);
		}
		System.out.println("Did you mean? : " + Arrays.asList(suggestions));
		System.out.println("Did you mean direct: " + ProcessingDao.sc.correct(Term));

		for (int i = 0; i < suggestions.length; i++)
			finalString += suggestions[i] + " ";

		returnList.add(finalString);
		return returnList;

	}

	/**
	 * A new independent service for crosslingual has been developed, deprecating
	 * the need of this function
	 * 
	 * @param lang
	 * @param searchElement
	 * @return Map of hi and en translation/transliteration
	 * @deprecated
	 */
	@GET
	@Path("/crosslingual/{lang}/{searchElement}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Map<String, String> getCrossLingual(@PathParam("lang") String lang,
			@PathParam("searchElement") String searchElement) {
		logger.log(Level.INFO, "path encountered: /crosslingual/{lang}/{searchElement}");

		try {
			searchElement = searchElement.replaceAll("%20", " ");
			return processingDao.getCrossLingualMapFromSearchQuery(lang, searchElement);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HashMap<>();
	}

	/**
	 * A new independent service for crosslingual has been developed, deprecating
	 * the need of this function
	 * 
	 * @param sourceLang
	 *            * @param targetLang
	 * @param searchElement
	 * @deprecated
	 * @return Map of IL and IL translation/transliteration
	 */
	@GET
	@Path("/crosslingual/{sourceLang}/{targetLang}/{searchElement}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Map<String, String> getInterCrossLingual(@PathParam("sourceLang") String sourceLang,
			@PathParam("targetLang") String targetLang, @PathParam("searchElement") String searchElement) {
		logger.log(Level.INFO, "path encountered: /crosslingual/{sourceLang}/{targetLang}/{searchElement}");

		try {
			searchElement = searchElement.replaceAll("%20", " ");
			return processingDao.getInterCrossLingualMapFromSearchQuery(sourceLang, targetLang, searchElement);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HashMap<>();
	}
}
