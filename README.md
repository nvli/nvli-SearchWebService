-- CDAC Pune -- 
 May 24, 2017
-- -- --- -- --

NVLI Webservice Build for HCDC Group by AAI Group

public functions over REST Api


@POST @Path("/basicSearch1") : public SearchForm getListOfResult
@POST @Path("/basicSearch/{pageNum}/{pageWin}") : public SearchForm getListOfResultFromSearchForm
@POST @Path("/vasSearch/{pageNum}/{pageWin}") : public SearchForm getListOfVasResultFromSearchForm
@POST @Path("/ENEW") : public HashMap<String, Long> getNewsCount
@POST @Path("/GOVW") : public HashMap<String, Long> getGovCount
@POST @Path("/WIKI") : public HashMap<String, Long> getWikiCount
@POST @Path("/WIKICOUNT/{langcode}") : public HashMap<String, Long> getWikiCountPerLang
@POST @Path("/VALUE") : public HashMap<String, Long> getVasCount
@POST @Path("/VALUECOUNT/{vasCode}") : public HashMap<String, Long> getVasCountPerVas
@POST @Path("/ENEWHOST") : public HashMap<String, String> getNewsHostFacetCount
@POST @Path("/ENEWIDCODE") : public HashMap<String, String> getNewsIdcodeFacetCount
@POST @Path("/GOVWHOST") : public HashMap<String, String> getGovwHostFacetCount
@POST @Path("/GOVWIDCODE") : public HashMap<String, String> getGovwIdcodeFacetCount
@POST @Path("/ENEWIDCODE/{code}") : public HashMap<String, String> getNewsIdcodeFacetCount
@POST @Path("/GOVWIDCODE/{code}") : public HashMap<String, String> getGovwIdcodeFacetCount
@GET @Path("/similar/{resourceType}/{Term}") : public List<String> getDidYouMean
@Deprecated @GET @Path("/crosslingual/{lang}/{searchElement}") : public Map<String, String> getCrossLingual
@Deprecated @GET @Path("/crosslingual/{sourceLang}/{targetLang}/{searchElement}") : public Map<String, String> getInterCrossLingual


User details: MetadataIntegratorUser : mit_user@123#

Use Rest Client for service check over http
Use cdac.nvli.client.ClientTester for testing CLientside output

Don't forget to put:
NE_DICTIONARY_LOCATION=/opt/NVLI_AllContent/NVLI_Hosting/Tomcat8/server_resources/ne_translation_dictionaries
TRANSLATION_DICTIONARY_LOCATION=/opt/NVLI_AllContent/NVLI_Hosting/Tomcat8/server_resources/translation_dictionaries
TRANSLITERATION_DICTIONARY_LOCATION=/opt/NVLI_AllContent/NVLI_Hosting/Tomcat8/server_resources/transliteration_lists
at these locations. 